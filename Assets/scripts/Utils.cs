﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEditor;

public class Utils : MonoBehaviour
{
    public static readonly string DEFAULT_ERROR_TITLE = "AN ERROR HAS OCCURRED: ";

    public static void HandleException(Exception e)
    {
        Debug.LogError(DEFAULT_ERROR_TITLE);
        Debug.LogException(e);
    }

    /// <summary>
    /// DEPRECATED
    /// </summary>
    /// <param name="e"></param>
    /// <param name="message"></param>
    /// <param name="errorTitle"></param>
    /// <param name="okBtnText"></param>
    public static void HandleExceptionWithDialog(Exception e, string message = "UNKNOWN ERROR", string errorTitle = "ERROR", string okBtnText = "Ok")
    {
        Utils.HandleException(e);
        //EditorUtility.DisplayDialog(errorTitle, message, okBtnText);
    }
}
