﻿//This script was made using referênce code
//font: https://catlikecoding.com/unity/tutorials/editor/custom-list/

using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEditor;
using Tutorial;

[CustomEditor(typeof(TutorialController))]
public class TutorialEditor : Editor
{

    private bool ui = false;
    private bool configuration = false;

    private SerializedProperty isPlayerTrigger;

    public void OnEnable()
    {
        isPlayerTrigger = serializedObject.FindProperty("playerIsTarget");
    }

    public override void OnInspectorGUI()
    {

        //syncronizes the serializableObject wuth the selected GameObject data
        serializedObject.Update();

        #region Ui Setup
        ui = EditorGUILayout.Foldout(ui, "Tutorial Interface");

        if (ui)
        {
            EditorGUILayout.PropertyField(serializedObject.FindProperty("text"), new GUIContent("Text Display Panel"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("hintText"), new GUIContent("Hint Text Display Panel"));
        }

        #endregion

        #region Configuration

        configuration = EditorGUILayout.Foldout(configuration, "Tutorial Configuration");

        if (configuration)
        {

            SerializedProperty trigger = serializedObject.FindProperty("trigger");

            EditorGUILayout.PropertyField(trigger, new GUIContent("Start on"));

            TutorialTriggers scriptTrigger = (TutorialTriggers)trigger.enumValueIndex;

            if (scriptTrigger != TutorialTriggers.keyPressed)
            {
                if(scriptTrigger != TutorialTriggers.bossStart) EditorGUILayout.PropertyField(isPlayerTrigger, new GUIContent("Is player Triggerer"));
                //if the player is not the triggerer
                if (!isPlayerTrigger.boolValue || scriptTrigger == TutorialTriggers.bossStart)
                {
                    SerializedProperty targets = serializedObject.FindProperty("targets");
                    EditorGUILayout.PropertyField(targets, new GUIContent("Triggerers"));
                    if (targets.isExpanded)
                    {
                        EditorGUI.indentLevel += 1;
                        EditorGUILayout.PropertyField(targets.FindPropertyRelative("Array.size"));
                        for (int i = 0; i < targets.arraySize; i++)
                        {
                            EditorGUILayout.PropertyField(targets.GetArrayElementAtIndex(i), GUIContent.none);
                        }
                        EditorGUI.indentLevel -= 1;
                    }
                }
            }
            else
            {
                EditorGUILayout.PropertyField(serializedObject.FindProperty("keyToPress"), new GUIContent("Trigger Key"));
            }

            EditorGUILayout.PropertyField(serializedObject.FindProperty("doDestroyOnEnd"), new GUIContent("Destroy when finished"));
        }

        #endregion

        ShowScript(serializedObject.FindProperty("script"));

        //enables the user to change properties
        serializedObject.ApplyModifiedProperties();
    }

    private void ShowScript(SerializedProperty script)
    {
        EditorGUILayout.PropertyField(script, new GUIContent("Tutorial Script"));

        if (script.isExpanded)
        {
            EditorGUI.indentLevel += 1;
            EditorGUILayout.PropertyField(script.FindPropertyRelative("Array.size"));
            for (int i = 0; i < script.arraySize; i++)
            {
                SerializedProperty currentObject = script.GetArrayElementAtIndex(i);

                EditorGUILayout.PropertyField(currentObject);
                if (currentObject.isExpanded)
                {
                    //tutorial screenplay speach in a text area
                    EditorGUILayout.LabelField("Speach");

                    SerializedProperty lineText = currentObject.FindPropertyRelative("Line");

                    //set default text area aspect, but wrap words to make line script writting easier
                    GUIStyle style = new GUIStyle(GUI.skin.textArea);
                    style.wordWrap = true;

                    //this lineText = input enables the user to edit the text in the textArea
                    lineText.stringValue = EditorGUILayout.TextArea(lineText.stringValue, style, new GUILayoutOption[] { GUILayout.ExpandHeight(true), GUILayout.MinHeight(50f)});

                    //draw wait for input bool
                    SerializedProperty waitInput = currentObject.FindPropertyRelative("waitForInput");
                    EditorGUILayout.PropertyField(waitInput, new GUIContent("Wait for Input"));

                    //if wait for input is true, add the hint text, otherwise hintText is not relevant
                    EditorGUI.indentLevel += 1;
                    if (waitInput.boolValue)
                    {
                        SerializedProperty hintText = currentObject.FindPropertyRelative("hintText");
                        EditorGUILayout.PropertyField(hintText, new GUIContent("Hint Text"));

                        //show hint text
                        EditorGUI.indentLevel += 1;
                        if (hintText.isExpanded)
                        {
                            EditorGUILayout.PropertyField(hintText.FindPropertyRelative("action"), new GUIContent("Hint action"));

                            //show the action key dropdown
                            SerializedProperty keys = hintText.FindPropertyRelative("keyToPress");
                            EditorGUILayout.PropertyField(keys, new GUIContent("Action keys"));

                            //show the action keys
                            if (keys.isExpanded)
                            {
                                EditorGUILayout.PropertyField(keys.FindPropertyRelative("Array.size"));
                                for (int k = 0; k < keys.arraySize; k++)
                                {
                                    EditorGUILayout.PropertyField(keys.GetArrayElementAtIndex(k));
                                }
                            }
                        }
                        EditorGUI.indentLevel -= 1;

                    }

                    //Show Commands dropdown
                    SerializedProperty commands = currentObject.FindPropertyRelative("commands");
                    EditorGUILayout.PropertyField(commands, new GUIContent("Commands"));

                    if (commands.isExpanded)
                    {
                        EditorGUILayout.PropertyField(commands.FindPropertyRelative("Array.size"));
                        for (int c = 0; c < commands.arraySize; c++)
                        {
                            GetCommandGUI(commands.GetArrayElementAtIndex(c), c);
                        }
                    }

                    EditorGUI.indentLevel -= 1;

                    

                }
            }
            EditorGUI.indentLevel -= 1;
        }
    }

    private void GetCommandGUI(SerializedProperty command, int index)
    {
        SerializedProperty commandType = command.FindPropertyRelative("command");
        CommandType currentType = (CommandType)commandType.enumValueIndex;//convert value to original enum
        string[] enumFriendlyNames = commandType.enumDisplayNames;

        //this int will controll the ui output
        //since most of the ui structure is the same( property field + array) this will tell wich one to draw
        //0 = field only, 1 = field + array, 2 array only, 3> don't draw anything
        int propertyStructure = 0;
        string[] field = new string[2] { "", "" };//0 = variable name, 1 = label
        string[] list = new string[2] { "targets", "Game Objects" };//0 = array name, 1 = label

        //Set the command dropdown name as the choosen command in hierarchy
        EditorGUILayout.PropertyField(command, new GUIContent(enumFriendlyNames[commandType.enumValueIndex]));

        if (command.isExpanded)
        {
            EditorGUI.indentLevel += 1;

            EditorGUILayout.PropertyField(commandType, new GUIContent("Command")); 

             SerializedProperty targets;
            switch (currentType)
            {
                case CommandType.move:

                    SerializedProperty isUiElement = command.FindPropertyRelative("isUiElement");
                    EditorGUILayout.PropertyField(isUiElement, new GUIContent("Is Target Ui"));

                    if (!isUiElement.boolValue)
                    { 
                        //set field properties
                        field[0] = "pos";
                        field[1] = "Position";

                        //use default list[] values

                    }
                    else
                    {
                        // set field properties
                        field[0] = "uiPos";
                        field[1] = "Rect Transform";

                        //use default list[] values

                    }

                    propertyStructure = 1;//draw a field+array

                    break;
                case CommandType.rotate:

                    //set field properties
                    field[0] = "rotation";
                    field[1] = "Rotation";

                    //use default list[] values

                    propertyStructure = 1;//draw a field+array

                    break;
                case CommandType.freezePlayer:

                    EditorGUILayout.PropertyField(command.FindPropertyRelative("doAction"), new GUIContent("Freeze"));

                    //set field properties
                    field[0] = "isUiElement";
                    field[1] = "Movement Only";

                    propertyStructure = 0;//draw a field only

                    break;
                case CommandType.setTime:

                    //set field properties
                    field[0] = "intensity";
                    field[1] = "Time scale";

                    propertyStructure = 0;//draw a field only

                    break;
                case CommandType.setGameObjectActive:

                    //set field properties
                    field[0] = "doAction";
                    field[1] = "Set Active";

                    //use default list[] values

                    propertyStructure = 1;//draw a field+array

                    break;
                case CommandType.setScriptActive:

                    EditorGUILayout.PropertyField(command.FindPropertyRelative("doAction"), new GUIContent("Set Active"));

                    SerializedProperty isPlayer = command.FindPropertyRelative("isUiElement");
                    EditorGUILayout.PropertyField(isPlayer, new GUIContent("Target Player"));

                    //is target is player, show string array
                    //because player is not created and so, cannot be directly referenced
                    if (isPlayer.boolValue)
                    {
                        //override default list[] values
                        list[0] = "names";
                        list[1] = "Script Names";
                    }
                    else//otherwise show behaviour array
                    {
                        //override default list[] values
                        list[0] = "scripts";
                        list[1] = "Scripts";
                    }

                    propertyStructure = 2;//draw a array only

                    break;
                case CommandType.addEffect:

                    SerializedProperty effects = command.FindPropertyRelative("effects");
                    EditorGUILayout.PropertyField(effects, new GUIContent("Effects"));

                    if (effects.isExpanded)
                    {
                        EditorGUI.indentLevel += 1;
                        EditorGUILayout.PropertyField(effects.FindPropertyRelative("Array.size"));
                        for (int o = 0; o < effects.arraySize; o++)
                        {
                            SerializedProperty curEffect = effects.GetArrayElementAtIndex(o);
                            EditorGUILayout.PropertyField(curEffect);

                            if (curEffect.isExpanded)
                            {
                                EditorGUILayout.PropertyField(curEffect.FindPropertyRelative("Array.Size"));
                                for(int e = 0; e < curEffect.arraySize; e++)
                                {
                                    EditorGUILayout.PropertyField(curEffect.GetArrayElementAtIndex(e));
                                }
                            }
                        }
                        EditorGUI.indentLevel -= 1;
                    }

                    //can't simplyfy all the code, since this case is very specific
                    //but the target list can still be optimized
                    propertyStructure = 2;//array only, add target list

                    break;
                default:
                    break;
            }

            switch (propertyStructure)
            {
                case 1://field + array

                    if (field[1] != "")//there is a prefered label, use it
                        EditorGUILayout.PropertyField(command.FindPropertyRelative(field[0]), new GUIContent(field[1]));
                    else//if not, use default
                        EditorGUILayout.PropertyField(command.FindPropertyRelative(field[0]));
                    goto case 2;
                case 2://array only

                    targets = command.FindPropertyRelative(list[0]);

                    if (list[1] != "")//there is a prefered label, use it
                        EditorGUILayout.PropertyField(targets, new GUIContent("Targets"));
                    else//if not, use default
                        EditorGUILayout.PropertyField(targets);
                    if (targets.isExpanded)
                    {
                        EditorGUI.indentLevel += 1;
                        EditorGUILayout.PropertyField(targets.FindPropertyRelative("Array.size"));
                        for (int o = 0; o < targets.arraySize; o++)
                        {
                            EditorGUILayout.PropertyField(targets.GetArrayElementAtIndex(o), GUIContent.none);
                        }
                        EditorGUI.indentLevel -= 1;
                    }
                    break;
                case 0://field only

                    if (field[1] != "")//there is a prefered label, use it
                        EditorGUILayout.PropertyField(command.FindPropertyRelative(field[0]), new GUIContent(field[1]));
                    else//if not, use default
                        EditorGUILayout.PropertyField(command.FindPropertyRelative(field[0]));

                    break;

                default: break;

            }

            EditorGUI.indentLevel -= 1;
        }
    }
}
