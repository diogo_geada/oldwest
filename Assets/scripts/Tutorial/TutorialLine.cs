﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tutorial
{
    [System.Serializable]
    public class TutorialLine
    {

        public string Line;
        public TutorialCommand[] commands;
        public bool waitForInput = true;
        public HintText hintText;

        public TutorialLine() { }

    }
}
