﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using Tutorial;
using UnityEngine.UI;

public class TutorialController : MonoBehaviour
{
    public TutorialTriggers trigger;
    public bool playerIsTarget = true;
    public GameObject[] targets;
    public Text text;
    public Text hintText;
    private string[] hintTextTemplate = new string[] { "Press ", " to " };
    public KeyType keyToPress;
    [SerializeField]
    private bool doDestroyOnEnd = true;
    private bool scriptPlaying = false;

    private int[] eventListIndex;

    public TutorialLine[] script;

    // Use this for initialization
    void Start()
    {
        eventListIndex = new int[targets.Length];
        int counter = 0;
        switch (trigger)
        {
            case TutorialTriggers.damageTaken:

                //player is created at runtime, so, this statement is needed to get a reference to player
                if (playerIsTarget) {
                    eventListIndex = new int[1];
                    eventListIndex[0] = PlayerManager.Instance.Health.onAddDmg.Count;
                    PlayerManager.Instance.Health.onAddDmg.Add(() => {
                        gameObject.GetComponent<TutorialController>().StartScript();
                    });
                }
                else {
                    
                    foreach (GameObject target in targets)
                    {
                        eventListIndex[counter] = PlayerManager.Instance.Health.onAddDmg.Count;
                        target.GetComponent<HealthController>().onAddDmg.Add(() => { StartScript(); });
                        counter++;
                    }
                }
                break;

            case TutorialTriggers.healthRestored:

                if (playerIsTarget)
                {
                    eventListIndex = new int[1];
                    eventListIndex[0] = PlayerManager.Instance.Health.onAddDmg.Count;
                    PlayerManager.Instance.Health.onAddHealth.Add(() => { StartScript(); });
                }
                else
                    foreach (GameObject target in targets)
                    {
                        eventListIndex[counter] = PlayerManager.Instance.Health.onAddHealth.Count;
                        target.GetComponent<HealthController>().onAddHealth.Add(() => { StartScript(); });
                        counter++;
                    }

                break;

            case TutorialTriggers.increaseMaxHealth:

                if (playerIsTarget)
                {
                    eventListIndex = new int[1];
                    eventListIndex[0] = PlayerManager.Instance.Health.onMaxHealthChange.Count;
                    PlayerManager.Instance.Health.onMaxHealthChange.Add(() => { StartScript(); });
                }
                else
                {
                    foreach (GameObject target in targets)
                    {
                        eventListIndex[counter] = PlayerManager.Instance.Health.onMaxHealthChange.Count;
                        target.GetComponent<HealthController>().onMaxHealthChange.Add(() => { StartScript(); });
                        counter++;
                    }
                }
                break;
            case TutorialTriggers.die:

                if (playerIsTarget)
                {
                    eventListIndex = new int[1];
                    eventListIndex[0] = PlayerManager.Instance.Health.onDeath.Count;
                    PlayerManager.Instance.Health.onDeath.Add(() => { StartScript(); });
                }
                else
                    foreach (GameObject target in targets)
                    {
                        eventListIndex[counter] = PlayerManager.Instance.Health.onDeath.Count;
                        target.GetComponent<HealthController>().onDeath.Add(() => { StartScript(); });
                        counter++;
                    }

                break;
            case TutorialTriggers.bossStart:
                foreach (GameObject target in targets)
                {
                    eventListIndex[counter] = target.GetComponent<BossController>().onBossStart.Count;
                    target.GetComponent<BossController>().onBossStart.Add(() => { StartScript(); });
                    counter++;
                }
                break;
            default: 
                break;

        }
    }

    public void StartScript()
    {
        StartCoroutine(ReadScript());
    }

    //Start script on trigger enter
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!isActiveAndEnabled) return;
        if (trigger == TutorialTriggers.triggerEnter)
        {
            if (playerIsTarget && collision.transform.tag == "Player")
            {
                StartScript();
            }
            else if (!playerIsTarget)
            {
                foreach (GameObject target in targets)
                {
                    //in case the collision happens with a child transform of the target, get his root
                    if (target == collision.transform.root.gameObject)
                    {
                        StartScript();
                        break;
                    }
                }
            }
        }
 
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (!isActiveAndEnabled) return;
        if(trigger == TutorialTriggers.keyPressed && collision.gameObject.tag == "Player" && !scriptPlaying)
        {
            if (Input.GetKeyDown(GameManager.keys[keyToPress]))
            {
                StartScript();
            }
        }
    }

    private IEnumerator ReadScript()
    {

        if (scriptPlaying) yield return null;
        else scriptPlaying = true;

        for (int i = 0; i < script.Length; i++)
        {

            //clear current screen text
            text.text = "";
            hintText.gameObject.SetActive(false);//hide int text

            //execute every command
            foreach (TutorialCommand command in script[i].commands)
            {
                yield return ExecuteCommand(command);//wait for the end of current command before executing next
            }

            //write the line in the screen
            int l = 0;
            while (l < script[i].Line.Length)
            {
                text.text += script[i].Line[l++];
                yield return new WaitForSecondsRealtime (0.05f);//make it framerate independent/not afecter by timescale
            }

            //this will stop players from skipping the whole tutorial when pressing one key one time
            yield return new WaitForSecondsRealtime(0.2f);

            hintText.text = CreateHintText(script[i]);

            //show hint text after text is written
            hintText.gameObject.SetActive(true);

            //this will give player time to read
            //when key pressed, the tutorial will continue
            if (script[i].waitForInput)
            {
                bool doBreak = false;

                if (script[i].hintText.keyToPress.Length > 0)
                {
                    while (!doBreak)
                    {

                        foreach (KeyType key in script[i].hintText.keyToPress)
                        {
                            if (Input.GetKeyDown(GameManager.keys[key]))
                            {
                                doBreak = true;
                                break;
                            }
                        }

                        if (doBreak) break;
                        yield return null;
                    }
                }
                else
                {
                    //if there aren't assigned keys, wait for the input of the default key
                    while (!Input.GetKeyDown(GameManager.keys[KeyType.interact]))
                    {
                        yield return null;
                    }
                }
            }

        }
        if (doDestroyOnEnd)
        {
            //delete event listners
            for (int i = 0; i < targets.Length; i++) {
                switch (trigger)
                {
                    case TutorialTriggers.bossStart:
                        targets[i].GetComponent<BossController>().onBossStart.RemoveAt(eventListIndex[i]);
                        break;
                    case TutorialTriggers.damageTaken:
                        targets[i].GetComponent<HealthController>().onAddDmg.RemoveAt(eventListIndex[i]);
                        break;
                    case TutorialTriggers.healthRestored:
                        targets[i].GetComponent<HealthController>().onAddHealth.RemoveAt(eventListIndex[i]);
                        break;
                    case TutorialTriggers.increaseMaxHealth:
                        targets[i].GetComponent<HealthController>().onMaxHealthChange.RemoveAt(eventListIndex[i]);
                        break;
                    case TutorialTriggers.die:
                        targets[i].GetComponent<HealthController>().onDeath.RemoveAt(eventListIndex[i]);
                        break;
                    default: break;
                }
            }

            Destroy(gameObject);
        }
        yield return null;
    }

    //Returns the hint text with all the necessary keys
    private string CreateHintText(TutorialLine line)
    {
        string keys = "";
        int keyCount = line.hintText.keyToPress.Length;

        for (int i = 0; i < keyCount; i++)
        {
            //add current key to keys string
            keys += "'" + GameManager.keys[line.hintText.keyToPress[i]].ToString() + "'";

            //Set an or between the 2 last elements
            //2 = index between 2 last elements
            if (keyCount >= 2 && i == keyCount - 2)
            {
                keys += " or ";
            }
            else if (i != keyCount - 1) keys += ", "; //don't add a comma after the last element

        }

        //if there are no keys, use the default key, interaction
        if (keyCount == 0) keys = "'" + GameManager.keys[KeyType.interact].ToString() + "'";

        return hintTextTemplate[0] + keys + hintTextTemplate[1] + line.hintText.action;
    }

    /// <summary>
    /// Executes a given command and affects the target objects
    /// The Script Reader will only continue its execution when this function returns a value, so
    /// this function can disable the waiForInput option when needed
    /// </summary>
    /// <param name="statment"></param>
    /// <returns></returns>
    private IEnumerator ExecuteCommand(TutorialCommand statment)
    {

        switch (statment.command)
        {
            case CommandType.setGameObjectActive:

                foreach (GameObject target in statment.targets)
                {
                    target.SetActive(statment.doAction);
                }

                break;

            case CommandType.setScriptActive:

                //if the target is the player transfrom strings into player behaviours that are based on monobehaviour
                if (statment.isUiElement)
                {
                    foreach (String target in statment.names)
                    {
                        //cast the result into a monobehaviour so that it can be controlled
                        MonoBehaviour behaviour = PlayerManager.Instance.Player.GetComponent(target) as MonoBehaviour;
                        behaviour.enabled = statment.doAction;
                    }
                }
                else//otherwise apply to the behaviours
                {

                    foreach (Behaviour target in statment.scripts)
                    {
                        target.enabled = statment.doAction;
                    }
                }

                break;
            case CommandType.move:
                foreach (GameObject target in statment.targets)
                {
                    if (statment.isUiElement)
                    {
                        RectTransform rect = target.GetComponent<RectTransform>();

                        //lower left corner
                        rect.offsetMin = new Vector2(statment.uiPos.xMin, statment.uiPos.height);

                        //upper right corner
                        //for some reason, this values get negative when are positive, so set then negative
                        rect.offsetMax = new Vector2( -statment.uiPos.width, -statment.uiPos.yMin);


                    }
                    else 
                        target.transform.position = statment.pos;
                }
                break;
            case CommandType.rotate:
                foreach (GameObject target in statment.targets)
                {
                    target.transform.position = statment.rotation;
                }
                break;
            case CommandType.scale:
                foreach (GameObject target in statment.targets)
                {
                    target.transform.localScale = statment.pos;
                }
                break;
            case CommandType.freezePlayer:

                //there needs to be a player reference already instantiated
                while (!PlayerManager.Instance.Controller)
                {
                    yield return null;
                }


                //this will enable ui users to clear both freeze types when not specified
                //always freeze movement, since complete freeze also freezes movement
                    PlayerManager.Instance.Controller.freezeMovment = statment.doAction;
                if (!statment.isUiElement) //if it is suposed to freeze only movement, don't freeze shooting too
                    PlayerManager.Instance.Controller.freeze = statment.doAction;

                break;
            case CommandType.setTime:
                Time.timeScale = statment.intensity;
                break;
            case CommandType.addEffect:

                foreach (GameObject target in statment.targets)
                {
                    //Action effect = EffectManager.GetEffect(statment.e);
                }

                break;
            default:
                Debug.LogWarning(statment.command + " not implemented");
                break;
        }
    }
}
