﻿using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class HintTextController : MonoBehaviour {

    public GameObject canvas;

    protected virtual void OnTriggerEnter2D(Collider2D col)
    {
        //deactivated scripts still execute onTrigger events, so stop it
        if (col.transform.tag == "Player" && isActiveAndEnabled)
        {
            ShowHintText();
        }
    }

    protected virtual void OnTriggerExit2D(Collider2D col)
    {
        //deactivated scripts still execute onTrigger events, so stop it
        if (col.transform.tag == "Player" && isActiveAndEnabled)
        {
            HideHintText();
        }
    }

    protected void HideHintText()
    {
        if (canvas) canvas.SetActive(false);
    }

    protected void ShowHintText()
    {
        if (canvas) canvas.SetActive(true);
    }

}
