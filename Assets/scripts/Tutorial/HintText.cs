﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tutorial
{
    [System.Serializable]
    public class HintText
    {
        public KeyType[] keyToPress = new KeyType[] { KeyType.interact };
        public string action = "continue";
    }
}
