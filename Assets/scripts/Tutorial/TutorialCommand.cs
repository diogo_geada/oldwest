﻿using UnityEngine;
using Effects;

namespace Tutorial
{
    [System.Serializable]
    public class TutorialCommand
    {
        public bool doAction = true;
        public CommandType command;
        public GameObject[] targets;
        public Behaviour[] scripts;
        public Effect[] effects;
        public float intensity;
        public string[] names;
        public Vector2 pos;
        public Rect uiPos;
        public bool isUiElement;
        public Vector3 rotation;
    }

}
