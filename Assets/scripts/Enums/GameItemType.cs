﻿public enum GameItemType
{
    Whiskey = 1,
    StrongWhiskey = 2,
    Bullet = 3,
    SlotUpgrade = 4,
    BulletProofVest = 5,
    SmallAbsinthe = 6,
    None = 0
}
