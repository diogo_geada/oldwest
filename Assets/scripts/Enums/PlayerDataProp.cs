﻿using System;
using System.Web;

public enum PlayerDataProp
{
    Level,
    Money,
    Weapons,
    Ammo,
    StoredAmmo,
    BagItemsList,
    BagItemStacksList,
    CurrentMaxSlots,
    ControllKeys,
    Health,
    MaxHealth
}
