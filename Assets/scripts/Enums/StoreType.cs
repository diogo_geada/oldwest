﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public enum StoreType
{
    Saloon = 1,
    GunShop = 2,
    GeneralStore = 3,
    None = 0
}

