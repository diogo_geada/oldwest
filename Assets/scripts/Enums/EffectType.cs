﻿
/// <summary>
/// The effect types that can be applied
/// 0 means doesn't need to be clear, like health regeneration
/// 1 means that needs to be clear/reverted like invincibility
/// </summary>
public enum EffectType
{
    maxHealth,
    ammo,
    slotUpgrade,
    health,
    invincibility,
    accuracy,
    speed,
    jump,
    //poison
    None
}
