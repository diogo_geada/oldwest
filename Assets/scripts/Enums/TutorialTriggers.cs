﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tutorial
{
    public enum TutorialTriggers
    {
        triggerEnter,
        keyPressed,
        damageTaken,
        healthRestored,
        increaseMaxHealth,
        die,
        kill,
        bossStart
        //buy
    }
}
