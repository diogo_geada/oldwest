﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour {

    public int damage = 1;
    public string IgnoreTag;
    public GameObject shooter;
    public float maxDistance;

    private Vector3 startingPosition;

    /// <summary>
    /// The change of two bullets colliding with each other
    /// </summary>
    public float accuracy = 0.5f;

    // Use this for initialization
    void Start () {
        startingPosition = transform.position;
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        if (Vector3.Distance(startingPosition, transform.position) >= maxDistance) Destroy(gameObject);
	}

    void OnTriggerEnter2D(Collider2D col)
    {
        float collide = Random.Range(0f, 1f);
        if (col.tag == "Player" || col.tag == "Enemy" || col.tag == "Cover")
        {
            //bullets will allways collide with active covers
            if (collide <= accuracy || col.tag == "Cover")
            {
                HealthController hpC;
                if (col.transform.parent == shooter) return;
                if (col.transform.parent != null) hpC = col.transform.parent.GetComponent<HealthController>();
                else hpC = col.transform.GetComponent<HealthController>();

                if(hpC.isActiveAndEnabled) hpC.AddDmg(damage, shooter);
                Destroy(gameObject);
            }
        }
        else if ( col.tag == "Bullet" )
        {
            //bullet collision will be 5% of shooters accuracy
            if ( collide <= ( accuracy * 0.05f ) )//make changes of bullets collision rare, so that cover is not op
            {
                if (col != null) Destroy(col);
                Destroy(gameObject);
            }
        }
        else if (col.tag != IgnoreTag) Destroy(gameObject);
    }
}
