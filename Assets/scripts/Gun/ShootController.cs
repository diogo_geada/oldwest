﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ShootController : MonoBehaviour
{

    public AudioClip shoot;
    public AudioClip shootWithoutBullets;
    public AudioClip reload;
    public AudioClip pickBullets;

    public float accuracyDefault = 0.5f;//from 0-1 will define the change of the shooter hitting the target
    public float accuracy;
    public float maxRange = 10.0f;
    private AudioSource source;

    public GameObject bullet;
    private SpriteRenderer bulletSprite;
    private Rigidbody2D rb;
    public Vector2 spawnPoint;

    public float distanceFromGun = 0.4f;
    public float bulletSpeed = 7f;
    public int clipMax = 6;
    [SerializeField]
    private int ammo;
    [SerializeField]
    private int storedAmmo;

    public float reloadTime = 2f;
    public float fireRate = 1f;//dellay between bullets

    public int direction = 1;

    public delegate void UpdateUI();
    public UpdateUI updateUI = null;

    public List<Action> onShootStart = new List<Action>();
    public List<Action> onShootEnd = new List<Action>();

    public int Ammo
    {
        get { return ammo; }
        set
        {
            ammo = value;

            updateUI?.Invoke();
        }
    }

    public int StoredAmmo
    {
        get { return storedAmmo; }
        set
        {
            storedAmmo = value;
            source.PlayOneShot(pickBullets);
            updateUI?.Invoke();
        }
    }

    private void Awake()
    {
        source = GetComponent<AudioSource>();
    }

    // Use this for initialization
    void Start()
    {
        if (gameObject.tag != "Player") return;
        ammo = Convert.ToInt32(PlayerStorage.GetPlayerProp(PlayerDataProp.Ammo));
        storedAmmo = Convert.ToInt32(PlayerStorage.GetPlayerProp(PlayerDataProp.StoredAmmo));

        //get accuracy
        accuracy = accuracyDefault;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Shoot()
    {
        if (!isActiveAndEnabled) return;
        //Call callback events
        TriggerEvent(onShootStart);

        if (ammo > 0) StartCoroutine(DoShoot());
        else
        {//if the players shoots without bullets, make it vulnerable
            source.PlayOneShot(shootWithoutBullets);

            //player will have to wait for animation to end
            //otherwise he would still be invincible while shootin in cover
            StartCoroutine(Delay(fireRate,()=> {//wait for shot to end
                TriggerEvent(onShootEnd);
            }));
        }
    }

    private IEnumerator Delay(float wait, Action callback)
    {
        yield return new WaitForSeconds(wait);
        callback();
    }

    private IEnumerator DoShoot()
    {

        source.PlayOneShot(shoot);
        yield return new WaitForSeconds(0.2f);
        GameObject curBullet = Instantiate(bullet, transform);
        BulletController bulletController = curBullet.GetComponent<BulletController>();
        bulletController.shooter = gameObject;
        bulletController.accuracy = accuracy;//set bullet hit chance to the players accuracy
        bulletController.maxDistance = maxRange;
        bulletSprite = bullet.GetComponentInChildren<SpriteRenderer>();
        rb = curBullet.GetComponent<Rigidbody2D>();
        curBullet.transform.localPosition = new Vector2(spawnPoint.x + (distanceFromGun * direction), spawnPoint.y);
        if (direction == -1) bulletSprite.flipY = true;
        else bulletSprite.flipY = false;
        curBullet.transform.parent = null;
        rb.velocity = new Vector2(bulletSpeed * direction, curBullet.transform.position.y);
        Ammo--;
        yield return new WaitForSeconds(fireRate);

        //Call callback events
        TriggerEvent(onShootEnd);
    }

    public void Reload()
    {
        if (!isActiveAndEnabled) return;
        StartCoroutine(DoReload());
    }

    private IEnumerator DoReload()
    {
        source.PlayOneShot(reload);
        yield return new WaitForSeconds(reloadTime);
        if (storedAmmo >= clipMax)
        {
            storedAmmo -= (clipMax - Ammo);
            Ammo = clipMax;

        }
        else
        {
            if (Ammo + storedAmmo > clipMax)
            {
                storedAmmo -= (clipMax - Ammo);
                Ammo = clipMax;
            }
            else
            {
                Ammo = storedAmmo;
                storedAmmo = 0;
            }
        }
    }

    private void TriggerEvent(List<Action> eventList)
    {
        foreach (Action callback in eventList)
        {
            callback();
        }
    }
}
