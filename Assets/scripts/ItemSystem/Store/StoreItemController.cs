
﻿// This is attached to each StoreItem prefab instance and .Init() at runtime by the StoreController.
// Its functionality is to inject the text and sprite on the StoreItem prefab model.
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class StoreItemController : MonoBehaviour
{
    public GameItem gameItem;

    public Sprite ItemSprite { get; private set; }

    // Use this for initialization
    void Start () {
	}

	// Update is called once per frame
	void Update () {
	}

    /// <summary>
    /// Inject the item's data into the prefab.
    /// </summary>
    /// <param name="item">The item instance</param>
    public void Init(GameItem item)
    {
        this.gameItem = item;

        // Inject text UI's
        Text[] textUIs = GetComponentsInChildren<Text>();

        for (ushort i = 0; i < textUIs.Length; ++i)
        {
            if (textUIs[i].name.Contains("Price"))
                textUIs[i].text = "$" + gameItem.Price.ToString();
            else if (textUIs[i].name.Contains("ItemName"))
            {
                if (gameItem.VisibleName == null || gameItem.VisibleName == "")
                    textUIs[i].text = gameItem.ItemType.ToString();
                else
                    textUIs[i].text = gameItem.VisibleName;
            }
        }

        this.ItemSprite = Resources.Load<Sprite>( Path.Combine("Sprites", item.SpriteName) );
        transform.GetChild(0).GetComponent<Image>().sprite = this.ItemSprite;
    }

    // This is a workaround the fact that a prefab losses all sight of other gameObjects, 
    // so I can't directly connect a click event to a StoreController.
    public void Buy()
    {
        StoreController[] allStores = FindObjectsOfType<StoreController>();
        for (ushort i = 0; i < allStores.Length; ++i)
        {
            if (allStores[i].storeType == gameItem.StoreType)
            {
                if (allStores[i].Buy( gameItem ))
                {
                    if (gameItem.oneTimeBuy)
                        this.Deactivate();
                }
                break;
            }
        }
    }

    private void Deactivate()
    {
        gameObject.GetComponent<Button>().interactable = false;
    }
}

