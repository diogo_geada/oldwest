﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using Effects;

[RequireComponent ( typeof(BoxCollider2D) )]
public sealed class StoreController : MonoBehaviour {
    public GameObject STORE_UI;
    public GameObject STORE_CONTENT_UI;

    public StoreType storeType;
    public string storeTitle;

    public Wallet playerWallet;

    public List<GameItem> SaloonItems;

    public List<GameItem> GunStoreItems;

	// Use this for initialization
	void Start () {
        BoxCollider2D boxColl = GetComponent<BoxCollider2D>();

        if (boxColl)
            boxColl.isTrigger = true;
    }

	// Update is called once per frame
	void Update ()
    {
	}

    #region PUBLIC METHODS

    public bool Buy(GameItem gameItem)
    {
        Effect effectType = gameItem.effect;
        float itemPrice = gameItem.Price;
        InventorySystem inventory = InventorySystem.Instance;
        playerWallet = PlayerManager.Instance.Controller.wallet;

        if (itemPrice <= playerWallet.Money)
        {
            if (gameItem.collectible)
            {
                if (inventory.AddItem( gameItem ))
                {
                    playerWallet.RemoveMoney( itemPrice );
                    return true;
                }
                return false;
            }
            else
            {

                if (EffectManager.ApplyEffect(effectType, PlayerManager.Instance.Player))
                {
                    playerWallet.RemoveMoney( itemPrice );
                    return true;
                }
                return false;
            }
        }

        return false;
    }

    public void OpenStore()
    {
        if (storeTitle == "" || storeTitle == null)
            storeTitle = storeType.ToString();

        STORE_UI.transform.GetChild(2).GetComponentInChildren<Text>().text = storeTitle;
        InjectStoreItems(storeType);
        Time.timeScale = 0;
        STORE_UI.SetActive(true);
        StartCoroutine( WaitForCloseInput() );
    }

    public void CloseStore()
    {
        this.DestroyAllStoreItems();
        STORE_UI.SetActive(false);
        Time.timeScale = 1;
        StopAllCoroutines();
    }

    #endregion

    #region PRIVATE METHODS

    private void InjectStoreItems(StoreType storeType)
    {
        GameObject storeItemBase = Resources.Load<GameObject>( Path.Combine("StoreItems", "StoreItem") );

        List<GameItem> storeItems;

        // This way it's possible to use the same method for different store types.
        if (storeType == StoreType.Saloon)
            storeItems = SaloonItems;
        else
            storeItems = GunStoreItems;

        for (ushort i = 0; i < storeItems.Count; ++i)
        {
            GameObject storeItem = Instantiate(storeItemBase, STORE_CONTENT_UI.transform);
            storeItem.GetComponent<StoreItemController>().Init( storeItems[i] );
            storeItem.transform.SetParent(STORE_CONTENT_UI.transform, false);
        }
    }

    private void DestroyAllStoreItems()
    {
        int childCount = STORE_CONTENT_UI.transform.childCount;
        for (ushort i = 0; i < childCount; ++i)
        {
            Destroy( STORE_CONTENT_UI.transform.GetChild(i).gameObject );
        }
    }

    #endregion

    #region EVENT HANDLERS

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.transform.tag == "Player" && Input.GetKeyDown(GameManager.keys[KeyType.interact]))
        {
            OpenStore();
        }
    }

    private IEnumerator WaitForCloseInput()
    {
        yield return new WaitForSecondsRealtime(0.1f);
        while (!Input.GetKeyDown( GameManager.keys[KeyType.interact]) )
        {
            yield return null;
        }

        CloseStore();
    }

    #endregion
}