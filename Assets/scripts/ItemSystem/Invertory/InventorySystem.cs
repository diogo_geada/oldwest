﻿/* 
 * Geada, I've created this singleton for the inventory system (the bag).
 * This is to inforce that it can be only one instance of the inventory system running.
 * More info (for me to): http://csharpindepth.com/articles/general/singleton.aspx
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

/// <summary>
/// The InventorySystem thread safe singleton.
/// <para />
/// <code>
/// Use: 
/// InventorySystem.Instance.[parameter/method];
/// </code>
/// </summary>
/// <remarks>THE INVENTORYSYSTEM IS NOT STABLE YET AND ANY OF THIS METHODS/PROPERTIES CAN CHANGE AT ANY MOMENT.</remarks>
public sealed class InventorySystem : MonoBehaviour
{
    #region SINGLETON CTOR

    private static InventorySystem instance;

    public static InventorySystem Instance
    {
        get
        {
            return instance;
        }
    }

    private void Awake()
    {
        if (InventorySystem.instance == null)
            instance = this;
        else if (InventorySystem.instance != this)
            Destroy(this);
    }
    #endregion

    private void Start()
    {
        SLOT_CONTENT_UI = INVENTORY_UI.transform.GetChild(1).gameObject;

        for (ushort i = 0; i < MAX_GAME_SLOTS; ++i)
        {
            this.Bag.Add( SLOT_CONTENT_UI.transform.GetChild(i).gameObject.GetComponent<InventorySlotController>() );
        }

        CurrentMaxSlots = Convert.ToInt32( PlayerStorage.GetPlayerProp( PlayerDataProp.CurrentMaxSlots ));
        this.UpdateActiveSlotsUI();
        this.PopulatePlayerSavedBag();
    }

    #region PROPERTIES

    public GameObject INVENTORY_UI;

    // In the future this will be a list of GameItemPack with the quantity. Generalizing with the interface for now.
    // private Dictionary<GameItemType, GameItemPack> itemPacks;
    public List<InventorySlotController> bag;

    public List<InventorySlotController> Bag
    {
        get { return bag; }
        private set
        {
            bag = value;
        }
    }

    private GameObject SLOT_CONTENT_UI;

    private int slotCount;

    public int SlotCount
    {
        get { return slotCount; }
        set
        {
            if (value < 0)
                slotCount = 0;
            else
                slotCount = value;
        }
    }

    public const int MAX_GAME_SLOTS = 10;

    public int CurrentMaxSlots { get; private set; } = 2;

    #endregion

    void Update()
    {
        if ( Input.GetKeyDown( GameManager.keys[KeyType.bag] ))
        {
            INVENTORY_UI.SetActive( !INVENTORY_UI.activeInHierarchy );
        }
    }

    #region PUBLIC METHODS

    /// <summary>
    /// Add an item to the bag. 
    /// <para/>
    /// <code>
    /// Use: 
    /// InventorySystem.Instance.AddItem(gameItem);
    /// </code>
    /// </summary>
    /// <param name="item"></param>
    /// <remarks>THE INVENTORYSYSTEM IS NOT STABLE YET AND ANY OF THIS METHODS/PROPERTIES CAN CHANGE AT ANY MOMENT.</remarks>
    public bool AddItem(IItem item)
    {
        InventorySlotController newSlot;

        switch (item.Stackable)
        {
            // GameItemStack.
            case true:
                // Item Stack exists.
                if (this.AddToSameSlotStackType( item.ItemType ))
                {
                    return true;
                }
                // Item Stack does not exist.
                else
                {
                    // TODO: Show message saying to buy more slots or that the thee player has reached the maximum amount of slots.
                    if (SlotCount >= CurrentMaxSlots || SlotCount >= MAX_GAME_SLOTS)
                        return false;

                    // Find empty slot.
                    newSlot = this.FindEmptySlot();
                    if (!newSlot || newSlot == new InventorySlotController())
                        return false;

                    newSlot.AddItemToEmptySlot( 
                        new GameItemStack(
                            item.ItemType,
                            item.EffectType,
                            item.Effect,
                            item.VisibleName,
                            item.Price,
                            item.SpriteName,
                            item.Collectible,
                            item.Stackable,
                            item.OneTimeBuy,
                            item.StoreType
                            )
                    );
                    ++this.SlotCount;
                    return true;
                }
            // GameItem.
            case false:
                newSlot = this.FindEmptySlot();
                if (!newSlot || newSlot == new InventorySlotController() || SlotCount >= CurrentMaxSlots)
                    return false;

                newSlot.AddItemToEmptySlot( item as GameItem );
                ++this.SlotCount;
                return true;
            default:
                return false;
        }
    }

    public bool UpgradeMaxSlotCount(int quantity = 2)
    {
        if (this.CurrentMaxSlots >= MAX_GAME_SLOTS || this.CurrentMaxSlots + quantity > MAX_GAME_SLOTS)
            return false;

        this.CurrentMaxSlots += quantity;
        this.UpdateActiveSlotsUI();
        return true;
    }
    
    public void SaveBagInPlayerStorage()
    {
        List<GameItem> currentBagGameItems = new List<GameItem>();
        List<GameItemStack> currentBagGameItemStacks = new List<GameItemStack>();

        // Get current slot items
        for (ushort i = 0; i < Bag.Count; ++i)
        {
            if (this.Bag[i].Item != null)
                currentBagGameItems.Add( this.Bag[i].Item );
            else if (this.Bag[i].ItemStack != null)
                currentBagGameItemStacks.Add( this.Bag[i].ItemStack );
        }

        PlayerStorage.UpdatePlayerData(PlayerDataProp.BagItemsList, currentBagGameItems);
        PlayerStorage.UpdatePlayerData(PlayerDataProp.BagItemStacksList, currentBagGameItemStacks);
        PlayerStorage.UpdatePlayerData(PlayerDataProp.CurrentMaxSlots, this.CurrentMaxSlots);
    }

    public void Clear()
    {
        for (ushort i = 0; i < Bag.Count; ++i)
        {
            Bag[i].ClearSlot();
        }
    }

    #endregion

    #region PRIVATE METHODS

    // DO NOT CHANGE THIS ALGORITHM.
    private void PopulatePlayerSavedBag()
    {
        PlayerDataModel playerData = PlayerStorage.GetAllPlayerData();
        List<GameItem> currentBagGameItems = playerData.BagItemsList;
        List<GameItemStack> currentBagGameItemStacks = playerData.BagItemStacksList;
        ushort gameItemStackIdx = 0;
        ushort gameItemsIdx = 0;
        bool added;

        for (ushort i = 0; i < this.Bag.Count; ++i)
        {
            added = false;

            if (!this.Bag[i].HasContent)
            {
                while (gameItemStackIdx < currentBagGameItemStacks.Count)
                {
                    this.Bag[i].AddItemToEmptySlot( currentBagGameItemStacks[gameItemStackIdx] );
                    gameItemStackIdx = ++gameItemStackIdx;
                    added = true;
                    break;
                }

                if (added)
                    continue;

                while (gameItemsIdx < currentBagGameItems.Count)
                {
                    this.Bag[i].AddItemToEmptySlot( currentBagGameItems[gameItemsIdx]);
                    gameItemsIdx = ++gameItemsIdx;
                    added = true;
                    break;
                }
            }
        }
    }

    private void UpdateActiveSlotsUI()
    {
        for (ushort i = 0; i < this.CurrentMaxSlots; ++i)
        {
            if (!this.Bag[i].IsActive)
                this.Bag[i].ActivateSlot();
        }
    }

    #endregion

    #region PRIVATE METHODS - UTILS/HELPERS

    /// <summary>
    /// Returns an empty instance of InventorySlotController. If there is no available instance, it returns an empty new InventorySlotController() class
    /// </summary>
    /// <returns></returns>
    private InventorySlotController FindEmptySlot()
    {
        InventorySlotController emptySlot = this.Bag.Find(i => !i.HasContent);
        if (emptySlot == null)
            return new InventorySlotController();
        else
            return emptySlot;
    }

    private bool AddToSameSlotStackType(GameItemType gameItemType)
    {
        for (ushort i = 0; i < this.Bag.Count; ++i)
        {
            if (this.bag[i].ItemStack != null) {
                if (this.Bag[i].ItemStack.itemType == gameItemType)
                {
                    if (this.Bag[i].AddItemToStack())
                        return true;
                }
            }
        }

        return false;
    }

    #endregion
}
