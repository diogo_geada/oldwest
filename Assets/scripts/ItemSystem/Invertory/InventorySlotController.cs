﻿// This is atached to the slot prefab
// Colors: Not active BG(new Color(120, 100, 90, 255)) and deactivate button.
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Effects;

public class InventorySlotController : MonoBehaviour {

    #region PROPERTIES

    public bool IsActive { get; private set; }

    public bool HasContent { get; private set; }

    public GameItem Item
    {
        get;
        private set;
    }

    public GameItemStack ItemStack { get; private set; }

    public Sprite ItemSprite { get; private set; }

    private Image slot_UI;
    private Image slotImage_UI;
    private Button slotButton_UI;
    private Text slotText_UI;

    private Sprite availableSlotSprite;
    private Sprite lockedSlotSprite;
    #endregion

    private void Awake()
    {
        this.IsActive = false;
    }

    // Use this for initialization
    void Start () {
    }

	// Update is called once per frame
	void Update () {

	}

    #region PUBLIC METHODS

    public void ActivateSlot()
    {
        if (this.HasContent)
            return;
        
        this.slot_UI = gameObject.GetComponent<Image>();
        this.slotImage_UI = gameObject.transform.GetChild(0).GetComponent<Image>();
        this.slotButton_UI = gameObject.GetComponentInChildren<Button>();
        this.slotText_UI = gameObject.transform.GetChild(1).GetComponent<Text>();

        slot_UI.sprite = Resources.Load<Sprite>("slot-available");
        slotImage_UI.color = new Color(255, 255, 255, 0);
        slotButton_UI.enabled = true;

        this.IsActive = true;
    }

    public void DeactivateSlot()
    {
        this.slot_UI.sprite = Resources.Load<Sprite>("slot-locked");
        slotImage_UI.color = new Color(255, 255, 255, 0);
        slotImage_UI.sprite = null;
        this.slotButton_UI.enabled = false;

        if (this.Item != null) this.Item = null;
        if (this.ItemStack != null) this.ItemStack = null;

        this.IsActive = false;
    }

    public bool AddItemToEmptySlot(IItem item)
    {
        if (item.Stackable)
        {
            if (this.ItemStack != null)
                return false;

            this.ItemStack = item as GameItemStack;
            this.slotText_UI.text = this.ItemStack.Quantity.ToString();
        }
        else
        {
            if (this.Item != null)
                return false;

            this.Item = item as GameItem;
        }

        this.ItemSprite = Resources.Load<Sprite>( Path.Combine("Sprites", item.SpriteName) );
        slotImage_UI.sprite = this.ItemSprite;
        slotImage_UI.color = new Color(156, 128, 94, 255);
        this.HasContent = true;
        return true;
    }

    public bool AddItemToStack(int quantity = 1)
    {
        if (this.ItemStack == null)
            return false;

        this.ItemStack.AddItem( quantity );
        if (!slotText_UI.gameObject.activeInHierarchy) slotText_UI.gameObject.SetActive(true);
        this.slotText_UI.text = this.ItemStack.Quantity.ToString();
        return true;
    }

    public void ClearSlot()
    {
        if (!this.HasContent)
            return;

        // slotButton_UI.interactable = false;
        slotImage_UI.color = new Color(255, 255, 255, 0);
        slotImage_UI.sprite = null;
        slotText_UI.gameObject.SetActive(false);

        if (this.Item != null) this.Item = null;
        if (this.ItemStack != null) this.ItemStack = null;
        this.HasContent = false;
        --InventorySystem.Instance.SlotCount;
    }

    #endregion

    #region EVENT HANDLERS

    // Consume OnClick.
    public void Consume()
    {
        if (!this.HasContent)
            return;

        if (this.Item != null)
        {
            switch (this.Item.effectType)
            {
                case EffectType.health:
                    goto case EffectType.slotUpgrade;
                case EffectType.maxHealth:
                    goto case EffectType.slotUpgrade;
                case EffectType.slotUpgrade:
                    if (!EffectManager.ApplyEffect(Item.Effect, PlayerManager.Instance.Player))
                        return;
                    break;
                default:
                    PlayerManager.Instance.Controller.AddEffect(this.Item.Effect);
                    break;
            }
            
            this.ClearSlot();
        }
        else if (this.ItemStack != null)
        {
            switch (this.ItemStack.effectType)
            {
                case EffectType.health:
                    goto case EffectType.slotUpgrade;
                case EffectType.maxHealth:
                    goto case EffectType.slotUpgrade;
                case EffectType.slotUpgrade:
                    if (!EffectManager.ApplyEffect(ItemStack.Effect, PlayerManager.Instance.Player))
                        return;
                    break;
                default:
                    PlayerManager.Instance.Controller.AddEffect(this.ItemStack.Effect);
                    break;
            }
            ItemStack.RemoveItem(1);

            this.slotText_UI.text = this.ItemStack.Quantity.ToString();
            if (this.ItemStack.Quantity <= 0)
                this.ClearSlot();
        }
        else
            return;
    }

    #endregion
}
