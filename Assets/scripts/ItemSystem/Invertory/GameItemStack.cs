﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Effects;

//Sprite sprite = Resources.Load("") as Sprite;
//InventorySystem.Instance.AddItem(new GameItemStack(GameItemType.Bullet, "fv", 0.0f, sprite));

public class GameItemStack : GameItem
{
    public GameItemStack(GameItemType gameItemType, EffectType effectType, Effect effect, string visibleName, float price, string spriteName, bool collectible, bool stackable, bool oneTimeBuy, StoreType storeType = StoreType.None, int quantity = 1) : base(gameItemType, effectType, effect, visibleName, price, spriteName, collectible, stackable, oneTimeBuy, storeType)
    {
        this.Quantity = quantity;
    }

    // DEPRECATED. DO NOT USE THIS CONSTRUCTOR.
    // This was deprecated because it gives errors on the JSON serialization.
    //
    //public GameItemStack(GameItem gameItem, int quantity = 1) : base (gameItem.itemType, gameItem.visibleName, gameItem.price, gameItem.spriteName, gameItem.collectible, gameItem.stackable, gameItem.oneTimeBuy, gameItem.storeType)
    //{
    //    this.Quantity = quantity;
    //}

    public int Quantity { get; private set; }

    public void AddItem(int quantity = 1)
    {
        this.Quantity += quantity;
    }

    public void RemoveItem(int quantity = 1)
    {
        this.Quantity -= quantity;
    }
}
