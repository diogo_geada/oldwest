﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Effects;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

[System.Serializable]
public class GameItem : IItem
{
    #region CONSTRUCTOR

    public GameItem(GameItemType gameItemType, EffectType effectType, Effect effect, string visibleName, float price, string spriteName, bool collectible, bool stackable, bool oneTimeBuy, StoreType storeType = StoreType.None)
    {
        this.itemType = gameItemType;
        this.effectType = effectType;
        this.effect = effect;
        this.visibleName = visibleName;
        this.price = price;
        this.spriteName = spriteName;
        this.storeType = storeType;
        this.collectible = collectible;
        this.stackable = stackable;
        this.oneTimeBuy = oneTimeBuy;
    }
    
    #endregion

    #region PROPERTIES

    // The fields should be private but are public to be seen on the Unity's GUI.
    public GameItemType itemType;
    public GameItemType ItemType
    {
        get
        {
            return itemType;
        }
    }

    public string visibleName;
    public string VisibleName
    {
        get
        {
            return visibleName;
        }
    }

    public float price;
    public float Price
    {
        get
        {
            return price;
        }
    }

    public string spriteName;
    public string SpriteName
    {
        get
        {
            return spriteName;
        }
    }

    public StoreType storeType;
    public StoreType StoreType
    {
        get
        {
            return storeType;
        }
    }

    public EffectType effectType;
    public EffectType EffectType
    {
        get
        {
            return effectType;
        }
    }

    public Effect effect;
    public Effect Effect
    {
        get
        {
            return effect;
        }
    }
    public bool collectible;
    public bool Collectible
    {
        get
        {
            return collectible;
        }
    }

    public bool stackable;
    public bool Stackable
    {
        get
        {
            return stackable;
        }
    }

    public bool oneTimeBuy;
    public bool OneTimeBuy
    {
        get
        {
            return oneTimeBuy;
        }
    }

    #endregion
}
