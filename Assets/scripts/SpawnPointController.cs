﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent( typeof( Collider2D ) )]
public class SpawnPointController : MonoBehaviour {

    void OnTriggerEnter2D(Collider2D col)
    {
        if(col.transform.tag == "Player")
        {
            GameManager.Instance.lastCheckpoint = transform.position;
            Destroy(gameObject);
        }
    }

}
