﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (EnemyController))]
public class EnemyHealthController : HumanoidHealthController {

    private EnemyController controller;


    [SerializeField]
    private float waitToDestroyOnDeath = 10f;

	// Use this for initialization
	protected override void Start () {

        base.Start();

        controller = GetComponent<EnemyController>();

        //i fplayer kills enemy, add bounty to player
        onAddDmg.Add(() => {

        });

        onDeath.Add(() => {

        float bounty = Random.Range(controller.bounty.x, controller.bounty.y);

        if (lastAttacker.tag == "Player") lastAttacker.GetComponent<PlayerController>().wallet.AddMoney(bounty);

            controller.bountyText.text = "$"+ bounty.ToString("F2");
            StartCoroutine(DestroyOnDeath());
        });

	}

    private IEnumerator DestroyOnDeath()
    {
        yield return new WaitForSeconds(waitToDestroyOnDeath);
        Destroy(gameObject);
    }

}
