﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealthController : HumanoidHealthController {

    public AudioClip increaseHealthSound;

    public string healthHUDName;
    public string maxHealthHUDName;

    #region ui
    public GameObject lifeIcon;
    public GameObject maxLifeIcon;
    private GameObject healthIconList;
    private GameObject maxHealthIconList;
    private HealthUiManager healthIcons = new HealthUiManager();
    private HealthUiManager maxHealthIcons = new HealthUiManager();
    #endregion

    // Use this for initialization
    protected override void Start () {

        base.Start();

        maxHealthIconList = UIManager.Instance.GetElement(maxHealthHUDName);
        healthIconList = UIManager.Instance.GetElement(healthHUDName);

        onAddHealth.Add(() => {
            source.PlayOneShot(increaseHealthSound);//play sound when health is added

            healthIcons.AddLife(Health);
                        
        });

        onAddDmg.Add(() => {
            healthIcons.RemoveLife(Health);
        });

        onMaxHealthChange.Add(() =>
        {
            int oldQuant = maxHealthIcons.CountActives();
            if (oldQuant <= MaxHealth)
            {
                int max = MaxHealth - oldQuant;
                for (int i = 0; i < max; i++)
                {
                    AddNewHealthIcon(maxLifeIcon, maxHealthIcons, maxHealthIconList);
                }
            }
            else ResetUi();
            
        });

        ResetUi();
    }

    /// <summary>
    /// Reset Player HUD
    /// </summary>
    private void ResetUi()
    {
        foreach (HealthIcon icon in healthIcons.icons)
        {
            Destroy(icon.life);
        }
        foreach (HealthIcon icon in maxHealthIcons.icons)
        {
            Destroy(icon.life);
        }
        healthIcons.icons = new List<HealthIcon>();
        maxHealthIcons.icons = new List<HealthIcon>();
        for (int i = 0; i < MaxHealth; i++)
        {
            AddNewHealthIcon(maxLifeIcon, maxHealthIcons, maxHealthIconList);
            if(i < Health) AddNewHealthIcon(lifeIcon, healthIcons, healthIconList);
        }

    }

    /// <summary>
    /// Adds a new health icon to HUD
    /// </summary>
    /// <param name="icon">icon game object</param>
    /// <param name="iconList">Health manager ui list</param>
    /// <param name="itemList">GmaeObject list</param>
    public void AddNewHealthIcon(GameObject icon, HealthUiManager iconList, GameObject itemList)
    {
        GameObject life = Instantiate(icon, itemList.transform);
        HealthIcon newIcon = new HealthIcon(life.GetComponent<Image>());
        newIcon.life = life;
        iconList.icons.Add(newIcon);
    }

}
