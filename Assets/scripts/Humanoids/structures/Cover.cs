﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Cover
{

    public GameObject cover { get; private set; }
    public coverController controller { get; private set; }
    private bool isCover = true;

    public float maxDistance = 0f;
    public float minDistance = 0f;

    public Vector3 Position
    {
        get
        {
            return cover.transform.position;
        }
    }

	public Cover(GameObject newCover, float minDistance)
	{
      
        try
        {
            cover = newCover;
            if(cover == null) {
				isCover = false;
			}else{
				controller = cover.GetComponent<coverController>();
				if (controller == null) isCover = false;
			}
            this.minDistance = minDistance;
        }
        catch (ArgumentException e)
        {
            isCover = false;
            Debug.LogError(e);
        }

    }

    /// <summary>
    /// Checks if the cover can be used
    /// </summary>
    /// <param name="enemyPos">Enemy position</param>
    /// <returns>True if it can be used</returns>
    public bool CanHide(Vector2 enemyPos)
    {
        if (!isCover) return false;

        //and if no one is hiding on it
        if (controller.Character == null)
        {
            //check if it is inside gun range
			
			float distance = Vector2.Distance(enemyPos, cover.transform.position);

            if (distance >= minDistance)
            {

                return true;

            }

        }

        return false;
    }

}
