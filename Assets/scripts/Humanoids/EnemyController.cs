﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Enums;

public class EnemyController : HumanoidController {

    public Vector2 bounty = new Vector2(0, 0);

    public float colliderRadiousOnDeath = 0.5f;

    private List<GameObject> coverObjects = new List<GameObject>();

    private Vector2 spawnPoint = new Vector2();
    public Vector2 SpawnPoint { get { return spawnPoint; } }

    public float maxDistanceFromSpawn = 10f;

    public Vector2 triggerSpeed = new Vector2(0.15f, 1);//bosses only;
    protected bool inCombat = false;
    protected bool makeDecision = true;//if this is true, the enemy has to make a decision, otherwhise, another action is already in course

	protected CoverState cover;
    public CoverState Cover {
        get
        {
            return cover;
        }
        set
        {
            cover = value;
        }
    }

    protected States currentState = States.idle;
    public GameObject canvas;
    public Text bountyText;

    public float walkSpeed = 3f;
    public float runSpeed = 5f;

    public bool InCombat {
        get {
            return InCombat;
        }

        set {
            inCombat = value;
            curState = 0;
        }
    }

    protected GameObject target = null;

    public List<States> CombatPattern = new List<States>();
    public List<States> IdlePattern = new List<States>();

    public float sightDistance = 15f;

    private int curState = 0;

    public Vector2 idleDuration = new Vector2(0.5f, 5f);

    public float safeDistance = 5f;//Wont get closed than this and if it is, it will run back
    private float combatRange = 10f;//Needs to be at maximum 10m from player to shoot him

    public enum States {
        shooting,
        runingToSafty,
        runingAfterTarget,
        gainingDistance,
        idle,
        walkingRandomly,
        reloading,
        takingCover
    }

    // Use this for initialization
    protected override void Start() {

        base.Start();

        combatRange = shootController.maxRange;
        spawnPoint = transform.position;
    }

    // Update is called once per frame
    void Update() {

        //if the enemy is in idle but the player has appeared, go into combat mode
        if (currentState == States.idle && !makeDecision && target != null)
        {
            if (Vector2.Distance(transform.position, target.transform.position) < safeDistance)
            {
                //if can't stop current coroutine, wait till it can
                while (!canStopCoroutines) { return; }
                StopAllCoroutines();
                curState++;
                if (curState >= IdlePattern.Count) curState = 0;
                makeDecision = true;
            }
        }

        //stops the enemy from overriding already ocurring actions
        if (!makeDecision) return;

        if (inCombat)
        {
            if(target == null)
            {
                inCombat = false;
                return;
            }
            if (Vector2.Distance(transform.position, target.transform.position) > sightDistance)
            {
                InCombat = false;
                target = null;
                makeDecision = true;
                return;
            }
            if (curState >= CombatPattern.Count) curState = 0;
            switch (CombatPattern[curState])
            {
                case States.shooting:
                    float distancetoTarget = Vector2.Distance(transform.position, target.transform.position);
                    if (distancetoTarget > combatRange)
                    {
                        StartCoroutine(MoveToCombat());
                    }
                    else if (distancetoTarget < safeDistance && cover != CoverState.covered)
                    {//if the character is hidden in a cover, he doesn't need to run away
                        StartCoroutine(RunToSafty());
                    }
                    else {
                        if (shootController.Ammo <= 0 && !Reloading && !Shooting)
                        {
                            StartCoroutine(ReloadGun());
                        }
                        else if(!Reloading && !Shooting)
                        {
                            StartCoroutine(ShootTarget());
                            curState++;
                        }
                    }
                    break;
                case States.gainingDistance:
                    StartCoroutine(RunToSafty());
                    curState++;
                    break;
                case States.idle:
                    StartCoroutine(Idle());
                    curState++;
                    break;
                case States.walkingRandomly:
                    StartCoroutine(MoveRandomly());
                    curState++;
                    break;
                case States.takingCover:
                    if (cover != CoverState.covered) StartCoroutine(TakeCover());
                    else curState++;
                    break;
            }
            if (curState >= CombatPattern.Count) curState = 0;
        }
        else {
            if (shootController.Ammo <= Mathf.Round(shootController.clipMax / 2))
            {
                StartCoroutine(ReloadGun());
                return;
            }
            if (curState >= IdlePattern.Count) curState = 0;
            switch (IdlePattern[curState])
            {
                case States.idle:
                    StartCoroutine(Idle());
                    break;
                case States.walkingRandomly: StartCoroutine(MoveRandomly());
                    break;
            }
            curState++;
            if (curState >= IdlePattern.Count) curState = 0;
        }

    }

    protected override void FixedUpdate()
    {
        base.FixedUpdate();
    }

    IEnumerator Idle()
    {
        makeDecision = false;
        currentState = States.idle;
        yield return new WaitForSeconds(Random.Range(idleDuration.x, idleDuration.y));
        makeDecision = true;
    }

    private IEnumerator ShootTarget() {
        makeDecision = false;
        canStopCoroutines = false;
        //if the players x Is > than enemy's x, player is at enemies left(wich is backward) and if the enemy is facing foward, he needs to flip
        if ((target.transform.position.x < transform.position.x && FacingForward) || (target.transform.position.x > transform.position.x && !FacingForward)) Flip();
        currentState = States.shooting;
        Shoot();
        yield return new WaitForSeconds(shootController.fireRate);
        makeDecision = true;
        canStopCoroutines = true;
    }

    private IEnumerator ReloadGun()
    {
        makeDecision = false;
        currentState = States.reloading;
        Reload();
        yield return new WaitForSeconds(shootController.reloadTime);
        Reloading = false;
        makeDecision = true;
    }

    private IEnumerator TakeCover()
    {
        if (cover == CoverState.covered)
        {
            ++curState;
            yield return null;
        }
        makeDecision = false;
        currentState = States.takingCover;
		cover = CoverState.searching;

        //if there is cover on left and right

        List<GameObject> coverObjectsCopy = new List<GameObject>(coverObjects);

        foreach (GameObject potentialCover in coverObjectsCopy)
        {

            Cover temp = new Cover(potentialCover, safeDistance);

            if(temp.CanHide(target.transform.position)){
                //This will ensure that the charater leaves the cover between him and the player
                Vector2 hidePosition = temp.Position;
                Vector2 playerToCover = new Vector2(target.transform.position.x, hidePosition.x);

                //sets the cover between the player and the character
                float dir = ( playerToCover.x > 0) ? 2f : -2f ;

                hidePosition.x += dir;

                bool jumpToNext = false;

                //walk to target
                while (!WalkTo(hidePosition))
                {

                    //in case the cover is not suitable to hide anymore, try to hide in the next one
                    //ex: Player or other enemy took cover on it while this was trying to reach it
                    if (!target) break;
                    if (!temp.CanHide(target.transform.position))
                    {

                        jumpToNext = true;
                        break;

                    }
                    else
                    {
                        yield return null;
                    }
                }

                if (jumpToNext) continue;

                //hide if possible
                //while the character is walking, the cover can be ocupied, so check before trying to hide
                if (temp.controller.Character == null)
                {
                    //try to hide
                    if (temp.controller.Hide(gameObject))
                    {
                        cover = CoverState.covered;
                    }
                    break;
                }
            }

        }


        curState++;
        if (cover == CoverState.searching)
        {
            cover = CoverState.exposed;
            StartCoroutine(MoveRandomly());
            yield return null;
        }

        makeDecision = true;
    }

    IEnumerator MoveToCombat()
    {
        makeDecision = false;
        currentState = States.runingAfterTarget;
        int dir = (transform.position.x > target.transform.position.x ) ? -1 : 1 ;
        if (Vector2.Distance(transform.position, target.transform.position) < combatRange*0.75f)
        {
            makeDecision = true;
            yield break;
        }
        while (Vector2.Distance(transform.position, target.transform.position) > combatRange*0.75f)
        {
            Move(dir);
            yield return null;
        }
        makeDecision = true;
    }

    IEnumerator RunToSafty()
    {
        makeDecision = false;
        currentState = States.runingToSafty;
        int dir = (transform.position.x < target.transform.position.x) ? 1 : -1;
        while (Vector2.Distance(transform.position, target.transform.position) < safeDistance*1.5f)
        {
            Move(-dir, runSpeed);//in the oposite direction
            yield return null;
        }
        makeDecision = true;
    }

    IEnumerator MoveRandomly()
    {
        makeDecision = false;
        currentState = States.walkingRandomly;
        int dir = Random.Range(0, 1);
        dir = (dir == 0) ? -1 : 1;
        float distance = Random.Range(-10f, 10f);
        Vector2 targetPoint = new Vector2(transform.position.x+(distance*dir), transform.position.y);
        float temp = 0;
        while (Vector2.Distance(transform.position, targetPoint) > 0.3f)
        {
            temp = Vector2.Distance(transform.position, targetPoint);
            if (Vector2.Distance(transform.position, spawnPoint) >= maxDistanceFromSpawn)//will not let the enemy wander in the whole map, will have a limited space he can wander
            {
                dir *= -1;//invert direction
                targetPoint = new Vector2(transform.position.x + (temp * dir), targetPoint.y);//set the remaining distance torwards the spawn point direction
                transform.position.Set(transform.position.x+(2*dir), transform.position.y, transform.position.z);
            }
            Move(dir, walkSpeed);
            yield return null;
        }
        makeDecision = true;
    }

    /// <summary>
    /// Walks the character to given point
    /// </summary>
    /// <param name="point">Coordinates to walk to</param>
    /// /// <param name="breakDistance">Stop at this distance from target</param>
    private bool WalkTo(Vector2 point, float breakDistance = 1.5f)
    {
        //the targets position - players position will return the distance
        //if it is positive, the dir is right, else it is left
        int dir = ( point.x - transform.position.x < 0 ) ? -1 : 1;

		point.y = transform.position.y;
		
        point.x += dir;//sets the point before or after the cover object
		 
        Move(dir);
		float distance = Vector2.Distance(transform.position, point) ;

        if (distance <= breakDistance) {
			rb.velocity = Vector2.zero;
			return true;
		}
        else return false;
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            while (!canStopCoroutines) { return; }
            StopAllCoroutines();

            target = col.gameObject;
            InCombat = true;
            makeDecision = true;

        }else if(col.gameObject.tag == "Cover")
        {
            //Set the y coordinate to the caracter coordinate to give distance on x axis
            Vector2 coverPos = col.transform.position;
            coverPos.y = transform.position.y;

            float distanceToCover = Vector2.Distance(transform.position, coverPos);
            bool added = false;//controlls if this cover object has been added

            //can't change a list during a loop, this copy will correct that
            List<GameObject> coverObjectsCopy = new List<GameObject>(coverObjects);
            foreach(GameObject potentialCover in coverObjectsCopy)
            {
                if(Vector2.Distance(transform.position, potentialCover.transform.position) > distanceToCover)
                {
                    coverObjects.Insert(coverObjects.IndexOf(potentialCover),col.gameObject);
                    added = true;
                }
            }

            //foreach will only add if the object distance is lower than an existing one, so in case it is bigger than the last element, add it to the list as the last element
            if(!added)coverObjects.Add(col.gameObject);
        }
        /*else if (col.gameObject.tag == "Bullet" && IsBoss && !inCombat && !bossFightStarted)
        {
            while (!canStopCoroutines) { return; }
            StopAllCoroutines();

            makeDecision = false;
            target = col.gameObject;
            if ((target.transform.position.x < transform.position.x && FacingForward) || (target.transform.position.x > transform.position.x && !FacingForward)) Flip();

            Shoot();
            makeDecision = true;
        }*/
    }

    void OnTriggerExit2D(Collider2D col)
    {
        if (col.gameObject.tag == "Cover") coverObjects.Remove(col.gameObject);
    }


    public override IEnumerator GetHurt() {
        yield return StartCoroutine(base.GetHurt());
        if (target == null) target = hpController.lastAttacker;
        StartCoroutine(ShootTarget());
    }

    protected override void Die()
    {
        base.Die();

        transform.GetChild(0).GetComponent<CircleCollider2D>().radius = colliderRadiousOnDeath;
        if (GetComponent<PickUp>()) GetComponent<PickUp>().enabled = true;
        canvas.SetActive(true);
    }

    public bool Reset()
    {
        target = null;
        inCombat = false;
        makeDecision = true;
        transform.position = spawnPoint;
        StopAllCoroutines();
        return true;
    }

}
