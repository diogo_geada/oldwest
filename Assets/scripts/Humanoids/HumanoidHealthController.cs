﻿using UnityEngine;
using System.Collections;

[RequireComponent( typeof( Animator ) ) ]
[RequireComponent( typeof( AudioSource ) ) ]
public class HumanoidHealthController : HealthController {

    private Animator anim;

    public delegate void SetTheHealth(int qnt);
    public SetTheHealth addLife = null;
    public SetTheHealth removeLife = null;

    

    protected AudioSource source;
    public AudioClip deathSound;

    private void Awake()
    {
        source = GetComponent<AudioSource>();
        anim = GetComponent<Animator>();
    }

    // Use this for initialization
    protected override void Start() {

        base.Start();

        onAddDmg.Add( () => { anim.SetBool("Hurt", true);});

    }

    public void PlayDeathSound()
    {
        source.PlayOneShot(deathSound);
    }
}
