﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using Effects;

[RequireComponent(typeof(ShootController))]
[RequireComponent(typeof(HealthController))]
[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(SpriteRenderer))]

public class HumanoidController : MonoBehaviour {

    //speed safeguard, so that speed can be reverted
    public float defaultSpeed = 5f;
    public float speed = 5f;
    public bool facingForward = true;
    protected bool canStopCoroutines = true;

    private bool isApplyingEffects = false;

    private List<Effect> effectList = new List<Effect>();


    protected bool FacingForward {
        get {
            return facingForward;
        }
    }

    private Animator anim;
    public ShootController shootController { get; protected set; }
    protected HealthController hpController;
    protected Rigidbody2D rb;
    private SpriteRenderer sprite;

    [SerializeField]
    private float distanceToGround = 1.0f;

    private bool grounded = true;
    public bool Grounded
    {
        get
        {
            return grounded;
        }
        set
        {
            grounded = value;
            anim.SetBool("Grounded", value);
        }
    }
    private bool jump;
    private bool falling = false;
    private bool shooting = false;
    private bool reloading = false;
    private bool hurt = false;
    private bool dead = false;

    public bool Jumping
    {
        get
        {
            return jump;
        }
        set
        {
            jump = value;
            anim.SetBool("Jump", value);
        }
    }

    public bool Falling
    {
        get
        {
            return falling;
        }

        set
        {
            falling = value;
            anim.SetBool("Falling", value);
        }
    }

    public bool Shooting
    {
        get
        {
            return shooting;
        }

        set
        {
            shooting = value;
            anim.SetBool("Shooting", value);
        }
    }

    public bool Reloading
    {
        get
        {
            return reloading;
        }

        set
        {
            reloading = value;
            //anim.SetBool("Shooting", value);
        }
    }

    public LayerMask ground;

    private void Awake()
    {
        shootController = GetComponent<ShootController>();
        hpController = GetComponent<HealthController>();
        sprite = GetComponent<SpriteRenderer>();
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
    }

    protected virtual void Start()
    {
       hpController.onAddDmg.Add( () => { Hurt(); } );
       hpController.onDeath.Add(() => { Die(); });
       speed = defaultSpeed;
    }

    protected virtual void FixedUpdate()
    {
        if (dead) return;
        Grounded = IsGrounded();//This will set the bool and the animation to the return value

        if (rb.velocity.y <= -0.1f)//if the player is falling, stop jumping
        {
            Jumping = false;
            Falling = true;
        }
        anim.SetFloat("Walk", Mathf.Clamp(rb.velocity.x, -1, 1));
        if (Grounded) Falling = false;
    }

    protected void Move(float intensity, float vel = 0)
    {
        if (hurt || dead) return;
        if ((facingForward && intensity < 0) || (!facingForward && intensity > 0)) Flip();
        if(vel == 0)rb.velocity = new Vector2(intensity * speed, rb.velocity.y);
        else rb.velocity = new Vector2(intensity * vel, rb.velocity.y);
    }

    /// <summary>
    /// IS the player colliding with the ground layer?
    /// </summary>
    /// <returns>boolean</returns>
    private bool IsGrounded()
    {
        if (dead) return true;
        RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.down, distanceToGround, ground);
        if (hit.collider != null) return true;
        else return false;
    }

    /// <summary>
    /// Forces a player jump and jump animation play
    /// </summary>
    protected void Jump()
    {
        if (hurt || dead) return;
        rb.AddForce(Vector2.up * 400f);
        anim.SetBool("Jump", true);
    }

    /// <summary>
    /// Flips the character sprite sheet
    /// </summary>
    protected void Flip()
    {
        if (dead) return;
        sprite.flipX = !sprite.flipX;
        facingForward = !sprite.flipX;
    }

    protected void Reload()
    {
        if (reloading || hurt || dead) return;
        else StartCoroutine(DoReload());
        
    }

    private IEnumerator DoReload()
    {
        reloading = true;
        shootController.Reload();
        canStopCoroutines = false;
        yield return new WaitForSeconds(shootController.reloadTime);
        canStopCoroutines = true;
        reloading = false;
    }

    protected void Shoot() {
        if (hurt || dead) return;
        StartCoroutine(DoShoot());
    }

    private IEnumerator DoShoot()
    {
        canStopCoroutines = false;
            Shooting = true;
            shootController.direction = (facingForward) ? 1 : -1;
            shootController.Shoot();
            yield return new WaitForSeconds(shootController.fireRate);
            Shooting = false;
        canStopCoroutines = true;
    }

    public void Hurt()
    {
        if (hurt || dead) return;
            hurt = true;
            StartCoroutine(GetHurt());
    }

    public virtual IEnumerator GetHurt()
    {
        canStopCoroutines = false;
        yield return new WaitForSeconds(0.01f);
        anim.SetBool("Hurt", false);
        yield return new WaitForSeconds(0.4f);
        hurt = false;
        canStopCoroutines = true;
    }

    protected virtual void Die()
    {
        StopAllCoroutines();
        StartCoroutine(Dead());
    }

    protected virtual IEnumerator Dead()
    {
        canStopCoroutines = false;
        dead = true;
        anim.SetBool("Dead", true);
        yield return new WaitForSeconds(0.2f);
        anim.SetBool("Dead", false);
        gameObject.layer = 9;
        rb.isKinematic = true;
        rb.velocity = Vector2.zero;
        GetComponent<CircleCollider2D>().enabled = false;
        enabled = false;
        canStopCoroutines = true;
    }

    public void AddEffect(Effect effect)
    {
        effectList.Add(effect);
        if (!isApplyingEffects) StartCoroutine(ApplyEffects());
    }

    private IEnumerator ApplyEffects()
    {
        isApplyingEffects = true;
        //if the effect is applied and has no duration, run it once
        List<Effect> effectsCopy = new List<Effect>(effectList);
        while (effectList.Count > 0)
        {
            effectsCopy = new List<Effect>(effectList);
            foreach (Effect effect in effectsCopy)
            {

                //returns an action, so call it
                //in case there is no intensity, like invincibility, don't call it per second
                if (!effect.applied)
                {
                    EffectManager.GetEffect(effect, gameObject)();
                    effect.applied = true;
                }
                else if (effect.intensity != 0)
                {
                    EffectManager.GetEffect(effect, gameObject)();

                }

                ++effect.timer;
                if (effect.timer >= effect.duration)
                {
                    //reset values that state effects change, ex.: invincibility, accuracy
                    if(effect.intensity == 0) EffectManager.GetEffect(effect, gameObject)();
                    effectList.Remove(effect);
                }
                yield return new WaitForSeconds(1);
            }
        }

        isApplyingEffects = false;
        yield return null;
    }

}
