﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;
using UnityEngine;

public class BossController : EnemyController
{

    public GameObject bossCam;
    private bool bossFightStarted = false;
    public string countDownUiName;

    public List<Action> onBossStart = new List<Action>();

    public string bossName;

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            while (!canStopCoroutines) { return; }
            StopAllCoroutines();
            target = col.gameObject;
            InCombat = true;
            if (!bossFightStarted)
            {
                StartCoroutine(StartBoss());
            }

        }
    }

    public IEnumerator StartBoss()
    {
        //call Events on boss start
        EventManager.ExecuteEvents(onBossStart);

        GetComponent<HealthController>().enabled = true;
        makeDecision = false;
        bossFightStarted = true;
        GameManager.Instance.SetBattleMusic();
        GameManager.Instance.GetComponent<BgScrollController>().enabled = false;
        currentState = States.idle;
        PlayerManager.Instance.Controller.freeze = true;
        bossCam.SetActive(true);
        int counter = 10;
        if (Camera.main != null) Camera.main.gameObject.SetActive(false);
        UIManager.Instance.GetElement(countDownUiName).SetActive(true);
        PlayerManager.Instance.Health.SetHealth(1);
        Text countdown = UIManager.Instance.GetElement(countDownUiName).transform.GetChild(0).GetComponent<Text>();
        while (counter > 1)
        {
            counter--;
            countdown.text = counter.ToString();
            yield return new WaitForSeconds(1);
        }
        UIManager.Instance.GetElement(countDownUiName).SetActive(false);
        PlayerManager.Instance.Controller.freeze = false;
        yield return new WaitForSeconds(UnityEngine.Random.Range(triggerSpeed.x, triggerSpeed.y));
        Shoot();
        inCombat = true;
        makeDecision = true;
    }

    protected override void Die()
    {
        base.Die();
        GameManager.Instance.KillBoss();
        PlayerManager.Instance.Health.enabled = false;
    }

}
