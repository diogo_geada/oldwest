﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : HumanoidController {

    #region UI Elements
    private Text bulletUI;
    private Text magazineUI;
    private Text walletUI;
    #endregion

    #region UI Element Name
    public string bulletHUDName;
    public string walletHUDName;
    #endregion

    public bool freeze = false;
    public bool freezeMovment = false;

    public GameObject gameOver;

    public Wallet wallet;

    protected override void Start()
    {
        base.Start();

        shootController.updateUI = ResetUI;

        float currentMoney = Convert.ToSingle(PlayerStorage.GetPlayerProp(PlayerDataProp.Money));
        wallet = new Wallet(currentMoney);

        wallet.onAdd = UpdateWallet;
        wallet.onRemove = UpdateWallet;

        walletUI = UIManager.Instance.GetElement(walletHUDName).transform.GetChild(0).GetComponent<Text>();
        magazineUI = UIManager.Instance.GetElement(bulletHUDName).transform.GetChild(2).GetComponent<Text>();
        bulletUI = UIManager.Instance.GetElement(bulletHUDName).transform.GetChild(1).GetComponent<Text>();

        UpdateWallet();
        ResetUI();
    }

    // Update is called once per frame
    void Update () {
        if (Input.GetKeyDown(GameManager.keys[KeyType.jump]) && Grounded && !freeze && !freezeMovment) {
            Jump();
        }
        else if (Input.GetKeyDown(GameManager.keys[KeyType.reload]) && Grounded && !freeze)
        {
            Reload();
        }
        if (Input.GetKeyDown(KeyCode.Space) && !freeze)
        {
            if (!Shooting && !Reloading) Shoot();
        }
    }

    protected override void FixedUpdate()
    {
        if(!freeze && !freezeMovment)
        {
            if (Input.GetKey(GameManager.keys[KeyType.moveRight]))
                Move(1);
            else if (Input.GetKey(GameManager.keys[KeyType.moveLeft]))
                Move(-1);
        }
        base.FixedUpdate();
    }

    public void ResetUI()
    {
        bulletUI.text = shootController.Ammo.ToString();
        magazineUI.text = shootController.StoredAmmo.ToString();
    }

    private void UpdateWallet()
    {
        walletUI.text = "$" + wallet.Money.ToString("F2");
    }

    protected override IEnumerator Dead()
    {
        yield return base.Dead();
        yield return new WaitForSeconds(3);
        GameManager.Instance.Respawn();
    }
}
