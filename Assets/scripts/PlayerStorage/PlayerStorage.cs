﻿/*
Example:

PlayerDataModel playerData = PlayerStorage.GetAllPlayerData();
Debug.Log(playerData.ControllKeys[KeyType.moveRight].ToString());
 
 */

using System;
using System.Text;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

public static class PlayerStorage
{
    public static readonly string STORAGE_PATH = Path.Combine("./", "Assets", "scripts", "PlayerStorage", "PlayerStorage.json");

    #region PUBLICS
    /// <summary>
    /// Returns a PlayerDataModel containing the player's data, from the PlayerData JSON storage. Note: WeaponName is stored as the enum index.
    /// </summary>
    /// <returns>JObject</returns>
    public static PlayerDataModel GetAllPlayerData()
    {
        return GetPlayerDataAsModel();
    }

    /// <summary>
    /// Get a property, as object, from the PlayerData JSON storage. Note: WeaponName is stored as the enum index.
    /// </summary>
    /// <param name="property">Enum PlayerDataProp relative to the PlayerDataModel.</param>
    /// <returns></returns>    
    public static object GetPlayerProp(PlayerDataProp property)
    {
        JObject jObject = ReadJsonFile();
        string userProp;

        try
        {
            userProp = property.ToString();
        }
        catch (Exception e)
        {
            Debug.LogException(e);
            return new object();
        }

        return jObject[userProp];
    }

    public static KeyCode GetControllKey(KeyType keyType)
    {
        JObject jObject = ReadJsonFile();
        return (KeyCode)Convert.ToInt32(jObject["ControllKeys"][keyType.ToString()]);
    }

    public static Dictionary<KeyType, KeyCode> GetAllControllKeys()
    {
        JObject jObject = ReadJsonFile();
        return jObject["ControllKeys"].ToObject<Dictionary<KeyType, KeyCode>>();
    }

    /// <summary>
    /// This overrides all the PlayerStorage json file. Use with caution.
    /// </summary>
    /// <param name="playerData"></param>
    public static void UpdateAllPlayerData(PlayerDataModel playerData)
    {
        try
        {
            WriteJsonFile( playerData );
        }
        catch (Exception e)
        {
            Debug.LogException(e);
        }
    }

    /// <summary>
    /// Update a property from PlayerStorage. Currently you can update: StoredAmmo, Ammo, Level, Money, Weapons, Health, MaxHealth, BagItemsList, BagItemStacksList.
    /// </summary>
    /// <param name="property">The property to update</param>
    /// <param name="value">The value of the property. Must be the same as in the PlayerDataModel.</param>
    public static void UpdatePlayerData(PlayerDataProp property, object value)
    {
        JObject jsonData = ReadJsonFile();

        PlayerDataModel playerData;

        if (jsonData.Count <= 0)
        {
            Debug.Log("CREATE NEW EMPTY JSON PLAYERSTORAGE FILE.");
            playerData = new PlayerDataModel();
            playerData = InsertDataOnModel(playerData, property, value);
            WriteJsonFile( playerData );
        }
        else
        {
            try
            {
                playerData = jsonData.ToObject<PlayerDataModel>();
                playerData = InsertDataOnModel(playerData, property, value);
                WriteJsonFile( playerData );
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }
        }
    }

    public static void AddWeapon(WeaponName weapon)
    {
        JObject jsonData = ReadJsonFile();
        PlayerDataModel playerData = jsonData.ToObject<PlayerDataModel>();
        playerData.Weapons.Add( weapon );
        WriteJsonFile( playerData );
    }

    public static void UpdatePlayerControlls(Dictionary<KeyType, KeyCode> controllKeys)
    {
        JObject jsonData = ReadJsonFile();
        PlayerDataModel playerData = jsonData.ToObject<PlayerDataModel>();
        playerData.ControllKeys = controllKeys;
        WriteJsonFile( playerData );
    }
    #endregion

    #region PRIVATES
    private static void WriteJsonFile(PlayerDataModel data)
    {
        try
        {
            using (StreamWriter fs = File.CreateText(STORAGE_PATH))
            {
                fs.Write( JsonConvert.SerializeObject(data, Formatting.Indented) );
            }
        }
        catch (Exception e)
        {
            Debug.LogException(e);
        }
    }

    /// <summary>
    /// It returns the player game data as a JObject.
    /// </summary>
    /// <returns></returns>
    private static JObject ReadJsonFile()
    {
        try
        {
            string jsonStringData = File.ReadAllText(STORAGE_PATH, Encoding.UTF8);
            return JObject.Parse( jsonStringData );

        }
        catch (Exception e)
        {
            Debug.LogException(e);
            return new JObject();
        }
    }

    private static PlayerDataModel GetPlayerDataAsModel()
    {
        JObject jsonData = ReadJsonFile();
        return jsonData.ToObject<PlayerDataModel>();
    }

    private static PlayerDataModel InsertDataOnModel(PlayerDataModel model, PlayerDataProp property, object value)
    {
        switch (property)
        {
            case PlayerDataProp.StoredAmmo:
                model.StoredAmmo = Convert.ToInt32( value );
                break;
            case PlayerDataProp.Ammo:
                model.Ammo = Convert.ToInt32( value );
                break;
            case PlayerDataProp.Level:
                model.Level = Convert.ToInt32( value );
                break;
            case PlayerDataProp.Money:
                model.Money = Convert.ToSingle( value );
                break;
            case PlayerDataProp.Weapons:
                List<WeaponName> listValue = value as List<WeaponName>;
                model.Weapons = listValue;
                break;
            case PlayerDataProp.Health:
                model.Health = Convert.ToInt32( value );
                break;
            case PlayerDataProp.MaxHealth:
                model.MaxHealth = Convert.ToInt32( value );
                break;
            case PlayerDataProp.CurrentMaxSlots:
                model.CurrentMaxSlots = Convert.ToInt32( value );
                break;
            case PlayerDataProp.BagItemsList:
                List<GameItem> itemList = value as List<GameItem>;
                model.BagItemsList = itemList;
                break;
            case PlayerDataProp.BagItemStacksList:
                List<GameItemStack> itemStackList = value as List<GameItemStack>;
                model.BagItemStacksList = itemStackList;
                break;
        }

        return model;
    }
    #endregion
}
