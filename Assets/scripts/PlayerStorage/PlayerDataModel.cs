﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class PlayerDataModel
{
    public int Level { get; set; }

    public float Money { get; set; }

    public int Ammo { get; set; }

    public int StoredAmmo { get; set; }

    public int Health { get; set; }

    public int MaxHealth { get; set; }

    public List<WeaponName> Weapons { get; set; }

    public Dictionary<KeyType, KeyCode> ControllKeys { get; set; }

    public List<GameItem> BagItemsList { get; set; }

    public List<GameItemStack> BagItemStacksList { get; set; }

    public int CurrentMaxSlots { get; set; }
}
