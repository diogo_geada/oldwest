﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class ItemsManager : MonoBehaviour {

    #region SINGLETON CTOR

    private static ItemsManager instance;

    public static ItemsManager Instance
    {
        get
        {
            return instance;
        }
        private set
        {
            instance = value;
        }
    }

    private void Awake()
    {
        if (ItemsManager.instance == null)
            Instance = this;
        else if (ItemsManager.instance != this)
            Destroy( this );
    }

    #endregion

    // Use this for initialization
    void Start () {

	}

	// Update is called once per frame
	void Update () {

	}

    #region PROPERTIES
    #endregion
}
