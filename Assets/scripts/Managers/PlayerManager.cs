﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Effects;

public class PlayerManager : MonoBehaviour {

    public static PlayerManager Instance;

    public GameObject playerPrefab;
    public Vector2 playerSpawnPoint;

    public GameObject Player{ get; private set; }
    public PlayerController Controller { get; private set; }
    public PlayerHealthController Health { get; private set; }
    public List<Behaviour> onPlayerReady = new List<Behaviour>();

	// Use this for initialization
	void Awake () {
        if (Instance == null) Instance = this;
        else if (Instance != this) Destroy(this);
	}

    void Start()
    {
        //spawn player on start
        SpawnPlayer();
    }

    public void SpawnPlayer()
    {
        Player = Instantiate(playerPrefab);
        //load player data
        Player.transform.position = playerSpawnPoint;
        Controller = Player.GetComponent<PlayerController>();
        Health = Player.GetComponent<PlayerHealthController>();

        GameManager.Instance.ActivateBehaviours(onPlayerReady);
    }


    // Update is called once per frame
    void Update () {
		
	}

}
