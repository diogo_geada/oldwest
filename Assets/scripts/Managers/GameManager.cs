﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

[RequireComponent ( typeof (PlayerManager) )]
[RequireComponent ( typeof ( UIManager ))]
public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    public GameObject player;

    #region Respawn
    public List<GameObject> enemies = new List<GameObject>();//to aid on respawn
    public Vector2 lastCheckpoint = Vector2.zero;
    #endregion

    public AudioClip battleMusic;

    public static Dictionary<KeyType, KeyCode> keys = new Dictionary<KeyType, KeyCode>();

    public List<BossController> BossCount = new List<BossController>();

    private bool isRespawning = false;

    // This bool is to be used, for example, when the user input an invalid key on the controlls manager.
    public static bool canPlay = true;

    /// <summary>
    /// 
    /// This stores the current player level (starting from the main scene at 1). To load a add 1. E.g.: SceneManager.LoadScene(PlayerLevel + 1)
    /// 
    /// </summary>
    private int CurrentPlayerLevel { get; set; }

    // Use this for initialization
    void Awake()
    {
        if (GameManager.Instance == null) Instance = this;
        else if (GameManager.Instance != this) Destroy(this);

        // Get the stored keys form PlayerStorage.
        GameManager.keys = PlayerStorage.GetAllControllKeys();
        this.CurrentPlayerLevel = Convert.ToInt32( PlayerStorage.GetPlayerProp( PlayerDataProp.Level ));
    }

    public void Replay()
    {
        if (!canPlay)
            return;

        Time.timeScale = 1;
        SceneManager.LoadScene( CurrentPlayerLevel + 1 );
    }

    public void MainMenu()
    {
        // PlayerStorage.UpdatePlayerData(PlayerDataProp.Level, 1);
        Time.timeScale = 1;
        SceneManager.LoadScene(0);
    }

    public void LoadTown()
    {
        SavePlayerProgress();
        SceneManager.LoadScene(1);
    }

    public void LoadNextLevel()
    {
        int playerLevel = Convert.ToInt32( PlayerStorage.GetPlayerProp(PlayerDataProp.Level) );
        PlayerStorage.UpdatePlayerData(PlayerDataProp.Level, ++playerLevel);
        SavePlayerProgress();
        SceneManager.LoadScene( playerLevel + 1 );
    }

    public void KillBoss()
    {
        StartCoroutine(GameWon());
    }

    public IEnumerator GameWon()
    {
        PlayerManager.Instance.Controller.freeze = true;
        yield return new WaitForSeconds(5);
        UIManager.Instance.GetElement("GameWon").SetActive(true);
    }

    public void SavePlayerProgress()
    {
        InventorySystem.Instance.SaveBagInPlayerStorage();
    }

    public void Exit()
    {
        Application.Quit();
    }

    public void SetBattleMusic()
    {
        GetComponent<AudioSource>().Stop();
        GetComponent<AudioSource>().PlayOneShot(battleMusic);
    }

    public void ActivateBehaviours(List<Behaviour> behaviours)
    {
        foreach(Behaviour script in behaviours)
        {
            script.enabled = true;
        }
    }

    public void Respawn()
    {
        if (isRespawning) return;
        isRespawning = true;
        StartCoroutine(doRespawn());
    }

    private IEnumerator doRespawn()
    {
        AudioSource source = GetComponent<AudioSource>();
        //reset evey enemy health and position
        List<GameObject> tmp = new List<GameObject>(enemies);

        source.Stop();

        foreach(GameObject enemy in tmp)
        {
            //if this enemy was destroyed
            if (enemy == null)
            {
                enemies.Remove(enemy);
                break;
            }
            else{
                HealthController enemyHealth = enemy.GetComponent<HealthController>();
                EnemyController enemyController = enemy.GetComponent<EnemyController>();

                //if the enemy is not died or destroyed
                if (enemyHealth.Health > 0)
                {
                    enemyHealth.ResetHealth();
                    enemyController.enabled = false;
                    enemy.transform.position = enemyController.SpawnPoint;

                    yield return enemy.GetComponent<EnemyController>().Reset();
                    enemyController.enabled = true;
                }
                else
                {
                    enemies.Remove(enemy);
                }
            }
    
        }

        Destroy(PlayerManager.Instance.Player);

        //in case there are still bullets on scene, stop them from hitting player on spawn
        yield return DestroyAllBullets();
        //save player info
        PlayerManager.Instance.playerSpawnPoint = lastCheckpoint;
        PlayerManager.Instance.SpawnPlayer();
        isRespawning = false;
        source.Play();
    }

    private int DestroyAllBullets()
    {
        GameObject[] bullets = GameObject.FindGameObjectsWithTag("Bullet");

        for (var i = 0; i < bullets.Length; i++)
        {
            Destroy(bullets[i]);
        }
        return 0;
    }

}
