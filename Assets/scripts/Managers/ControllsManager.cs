﻿// This script is attached to the Controlls GameObject.
// TODO: Parse the KeyDown event and not the UI input text.
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public sealed class ControllsManager : MonoBehaviour {

    #region PROERTIES

    public InputField WALK_LEFT_INPUT;
	public InputField WALK_RIGHT_INPUT;
	public InputField JUMP_INPUT;
	public InputField INTERACT_INPUT;
	public InputField SHOOT_INPUT;
	public InputField RELOAD_INPUT;
    public InputField BAG_INPUT;

    private KeyCode lastKeyPressed = KeyCode.None;

    #endregion

    #region SINGLETON CTOR

    private static ControllsManager _instance;
    public static ControllsManager Instance
    {
        get
        {
            return _instance;
        }
    }

    //used to initialize and setup
    void Awake() {
        if (ControllsManager._instance == null)
            _instance = this;
        else if (ControllsManager._instance != this)
            Destroy(this);
    }

    #endregion

    // Use this for initializationWalkLeftKey_EndEdit
    private void Start()
    {
        this.UpdateControllsUI();

        // Add dynamic event listeners the the controlls UI inputs.
		WALK_LEFT_INPUT.onEndEdit.AddListener( key => this.UpdateControlKey(KeyType.moveLeft) );
        WALK_RIGHT_INPUT.onEndEdit.AddListener( key => this.UpdateControlKey(KeyType.moveRight) );
        JUMP_INPUT.onEndEdit.AddListener( key => this.UpdateControlKey(KeyType.jump) );
        INTERACT_INPUT.onEndEdit.AddListener( key => this.UpdateControlKey(KeyType.interact) );
        SHOOT_INPUT.onEndEdit.AddListener( key => this.UpdateControlKey(KeyType.shoot) );
        RELOAD_INPUT.onEndEdit.AddListener( key => this.UpdateControlKey(KeyType.reload) );
        BAG_INPUT.onEndEdit.AddListener( key => this.UpdateControlKey(KeyType.bag) );
    }

	// Update is called once per frame
	void Update () {

	}

    #region PRIVATE METHODS - Update Methods

    private void UpdateControlKey(KeyType keyType)
	{
        switch (keyType)
        {
            case KeyType.moveLeft:
                GameManager.keys[KeyType.moveLeft] = lastKeyPressed;
                break;
            case KeyType.moveRight:
                GameManager.keys[KeyType.moveRight] = lastKeyPressed;
                break;
            case KeyType.jump:
                GameManager.keys[KeyType.jump] = lastKeyPressed;
                break;
            case KeyType.interact:
                GameManager.keys[KeyType.interact] = lastKeyPressed;
                break;
            case KeyType.shoot:
                GameManager.keys[KeyType.shoot] = lastKeyPressed;
                break;
            case KeyType.reload:
                GameManager.keys[KeyType.reload] = lastKeyPressed;
                break;
            case KeyType.bag:
                GameManager.keys[KeyType.bag] = lastKeyPressed;
                break;
            default:
                break;
        }

        this.UpdateControllsUI();
        PlayerStorage.UpdatePlayerControlls( GameManager.keys );
        this.StartListeningForKeyDown();
    }

    private void UpdateControllsUI()
    {
        WALK_LEFT_INPUT.text = GameManager.keys[KeyType.moveLeft].ToString();
        WALK_RIGHT_INPUT.text = GameManager.keys[KeyType.moveRight].ToString();
        JUMP_INPUT.text = GameManager.keys[KeyType.jump].ToString();
        INTERACT_INPUT.text = GameManager.keys[KeyType.interact].ToString();
        SHOOT_INPUT.text = GameManager.keys[KeyType.shoot].ToString();
        RELOAD_INPUT.text = GameManager.keys[KeyType.reload].ToString();
        BAG_INPUT.text = GameManager.keys[KeyType.bag].ToString();
    }

    #endregion


    #region EVENTS
    private bool noKeyPressed = true;

    public void StartListeningForKeyDown()
    {
        noKeyPressed = true;
        StartCoroutine( this.ListenForKeyDown() );
    }


    private IEnumerator ListenForKeyDown()
    {
        yield return new WaitForSecondsRealtime(0.1f);

        while (noKeyPressed)
        {
            foreach (KeyCode keyCode in Enum.GetValues( typeof(KeyCode) ))
            {
                if(Input.GetKeyDown( keyCode ) && !Input.GetKeyDown(KeyCode.Mouse0))
                {
                    this.lastKeyPressed = keyCode;
                    noKeyPressed = false;
                }
            }

            yield return null;
        }
    }

    #endregion

    #region PRIVATES - Class Utils/Helpers
    // DEPRECATED
    /// <summary>
    /// It parses an input key and, if suceeded, it also updates the UI controlls menu. If it doesn't succeed, it shows a pop up dialog and blocks further gameplay.
    /// </summary>
    /// <param name="key">The input key from the user</param>
    /// <param name="inputField">InputField UI Element to be updated</param>
    /// <returns></returns>
    /// <remarks>THIS WAS DEPRECATED.</remarks>
    private static KeyCode ParseKeyCode(string key, InputField inputField)
    {
        try
        {
            KeyCode keyCode = (KeyCode)Enum.Parse(typeof(KeyCode), key.ToUpper());
            inputField.text = key.ToUpper();
            GameManager.canPlay = true;
            return keyCode;
        }
        // Invalid KeyCode.
        catch (ArgumentException e)
        {
            Utils.HandleExceptionWithDialog(e, "Invalid Key. Please try again.");
            inputField.text = "";
            GameManager.canPlay = false;
            return KeyCode.None;
        }
        // Unknown exception.
        catch (Exception e)
        {
            Utils.HandleExceptionWithDialog(e);
            inputField.text = "Invalid Key. Please try again.";
            GameManager.canPlay = false;
            return KeyCode.None;
        }
    }
    #endregion
}
