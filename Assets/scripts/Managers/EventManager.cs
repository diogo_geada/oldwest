﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class EventManager{

    public static void ExecuteEvents(List<Action> events)
    {
        foreach(Action callback in events)
        {
            callback();
        }
    }

}
