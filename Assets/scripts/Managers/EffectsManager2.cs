﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectsManager2 : MonoBehaviour {

    #region SINGLETON CTOR

    private static EffectsManager2 instance;

    public static EffectsManager2 Instance
    {
        get
        {
            return instance;
        }
    }

    private void Awake()
    {
        if (EffectsManager2.instance == null)
            instance = this;
        else if (EffectsManager2.instance != this)
            Destroy(this);
    }

    #endregion

    #region PROPERTIES

    #endregion

    // Use this for initialization
    void Start () {
    }

	// Update is called once per frame
	void Update () {

	}

    public bool ApplyAffect(EffectType effectType)
    {
        switch (effectType)
        {
            case EffectType.health:
                if (PlayerManager.Instance.Player.GetComponent<HealthController>().AddHp(1))
                    return true;
                break;
            case EffectType.maxHealth:
                if (PlayerManager.Instance.Health.IncreaseMaxHealth())
                    return true;
                break;
            case EffectType.slotUpgrade:
                if (InventorySystem.Instance.UpgradeMaxSlotCount())
                    return true;
                break;
            case EffectType.ammo:
                ++PlayerManager.Instance.Controller.shootController.Ammo;
                return true;
        }

        return false;
    }

}
