﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Effects;

public class EffectManager
{

    public static Action GetEffect(Effect effect, GameObject objectToApply)
    {
        switch (effect.type)
        {
            case EffectType.invincibility:
                return () =>
                {
                    if (!effect.applied)
                        objectToApply.GetComponent<HealthController>().isInvincible = true;
                    else
                        objectToApply.GetComponent<HealthController>().isInvincible = false;
                };
            case EffectType.health:
                HealthController controller = objectToApply.GetComponent<HealthController>();
                int val = Int32.Parse((effect.intensity).ToString());

                return () =>
                {
                    if (effect.intensity < 0)
                    {
                        //the negative intensity would make addDmg function add health, so, invert it
                        //add damage has event listners related to the damage and death function, so,
                        //health method can't be used to decrease health
                        val *= -1;
                        controller.AddDmg(val);
                    }
                    else
                    {
                        controller.AddHp(val);
                    }
                };

            case EffectType.accuracy:

                ShootController stController = objectToApply.GetComponent<ShootController>();

                return () =>
                {
                    if (!effect.applied)
                        stController.accuracy += effect.intensity;
                    else
                        stController.accuracy = stController.accuracyDefault;
                };

            case EffectType.speed:

                HumanoidController hController = objectToApply.GetComponent<HumanoidController>();

                return () =>
                {
                    //reset speed
                    if (!effect.applied)
                        hController.speed = hController.defaultSpeed;
                    else hController.speed = effect.intensity;
                };
            // TODO: Add a tooltip on the store's strong whiskey button to tell the user that it increases his max life.
            // TODO: Change strong whiskey's sprite.
            case EffectType.maxHealth:
                return () =>
                {
                    PlayerManager.Instance.Health.IncreaseMaxHealth();
                };
            default: return () => { };
        }
    }

    public static bool ApplyEffect(Effect effect, GameObject targetObject)
    {
        switch (effect.type)
        {
            case EffectType.slotUpgrade:

                if (!InventorySystem.Instance.UpgradeMaxSlotCount((int)Math.Round(effect.intensity)))
                {
                    return false;
                }
                else return true;

            case EffectType.ammo:

                PlayerManager.Instance.Controller.shootController.StoredAmmo += Int32.Parse(effect.intensity.ToString());
                return true;
            case EffectType.health:

                HealthController controller = targetObject.GetComponent<HealthController>();
                int val = Int32.Parse((effect.intensity).ToString());
                if (controller.Health+val > controller.MaxHealth) return false;

                if (effect.intensity < 0)
                    {
                        //the negative intensity would make addDmg function add health, so, invert it
                        //add damage has event listners related to the damage and death function, so,
                        //health method can't be used to decrease health
                        val *= -1;
                        controller.AddDmg(val);
                    }
                    else
                    {
                        controller.AddHp(val);
                    }
                return true;
            case EffectType.maxHealth:

                HealthController healthController = targetObject.GetComponent<HealthController>();
                int val1 = Int32.Parse((effect.intensity).ToString());

                if (healthController.MaxHealth + val1 >= healthController.HealthLimit) return false;
                else
                    healthController.IncreaseMaxHealth();
                return true;
            default: return false;
        }
    }

}
