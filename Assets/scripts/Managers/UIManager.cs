﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour {

    public static UIManager Instance;

    [SerializeField]
    public List<UIElement> canvas = new List<UIElement>();

    // Use this for initialization
    void Awake()
    {
        if (Instance == null) Instance = this;
        else if (Instance != this) Destroy(this);
    }


    // Update is called once per frame
    void Update () {
		
	}

    public GameObject GetElement(string elementName)
    {
        foreach(UIElement element in canvas)
        {
            if (element.Name == elementName) return element.element;
        }

        Debug.LogError("Couldn't find request ui element: "+elementName);
        return null;
    }

    public void SetWindowActive(string windowName)
    {
        if (!GameManager.canPlay)
            return;

        GameObject element = GetElement(windowName);

        if (windowName == "Controlls" && !element.activeInHierarchy)
        {
            ControllsManager.Instance.StartListeningForKeyDown();
        }
        else if (windowName == "Controlls" && element.activeInHierarchy)
        {
            StopAllCoroutines();
        }

        element.SetActive(!element.activeInHierarchy);
    }

    public void SetWindowActive(GameObject window)
    {
        if (!GameManager.canPlay)
            return;

        window.SetActive(!window.activeInHierarchy);
    }
}
