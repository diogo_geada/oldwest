﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class HealthUiManager
{
    [SerializeField]
    public List<HealthIcon> icons = new List<HealthIcon>();

    public int CountActives()
    {

        int counter = 0;
        foreach (HealthIcon icon in icons)
        {
            if (icon.IsActive) counter++;
        }
        return counter;
    }

    public void AddLife(int val)
    {
        val -= CountActives();
        for (int i = 0; i < val; i++)
        {
            int quant = CountActives();
            if (quant < icons.Count) icons[quant + i].IsActive = true;
        }
    }

    public void RemoveLife(int val)
    {
        int quant = CountActives();
        val = quant-val;

        for (int i = 0; i < val; i++)
        {
            if (quant == 0) break;

            if (quant > 0)
            {

                icons[quant - (1 + i)].IsActive = false;

            }
        }
    }

    public void Empty()
    {
        foreach (HealthIcon icon in icons)
        {
            icons.Remove(icon);
        }
    }

    public HealthUiManager() {
        icons = new List<HealthIcon>();
    }
}
