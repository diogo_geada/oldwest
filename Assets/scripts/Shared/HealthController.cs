﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[RequireComponent( typeof (AudioSource) )]
public class HealthController : MonoBehaviour {

    [SerializeField]
    private int health;
    [SerializeField]
    private int maxHealth = 3;
    public bool isInvincible = false;

    protected int healthLimit = 10;
    public int HealthLimit { get { return healthLimit; } }


    public GameObject lastAttacker;

    public List<Action> onAddHealth = new List<Action>();
    public List<Action> onMaxHealthChange = new List<Action>();
    public List<Action> onAddDmg = new List<Action>();
    public List<Action> onDeath = new List<Action>();

    public int Health
    {
        get
        {
            return health;
        }
    }

    public int MaxHealth
    {
        get
        {
            return maxHealth;
        }
        set
        {
            maxHealth = value;
            health = maxHealth;
            EventManager.ExecuteEvents (onMaxHealthChange);
        }
    }


    // Use this for initialization
    protected virtual void Start()
    {
        health = maxHealth;
    }

    public bool AddHp(int value)
    {

        if (health < maxHealth)
        {
            if (health + value > maxHealth && health < maxHealth)
            {
                health = maxHealth;
            }
            else health += value;

            //call events on add Health
            EventManager.ExecuteEvents(onAddHealth);
            return true;
        }
        else return false;
    }

    public void AddDmg(int value, GameObject attacker = null)
    {
        lastAttacker = attacker;

        if (isInvincible) return;

        if (health - value <= 0)
        {
            health = 0;

            //call events on addDamage
            EventManager.ExecuteEvents(onDeath);
        }
        else
        {
            health -= value;

            //call events on addDamage
            EventManager.ExecuteEvents(onAddDmg);
        }

    }

    public bool IncreaseMaxHealth()
    {
        if (maxHealth < healthLimit)
        {
            MaxHealth++;
            return true;
        }
        else return false;
    }

    public void SetHealth(int quant)
    {
        MaxHealth = quant;
    }

    public void ResetHealth()
    {
        health = maxHealth;
    }

}
