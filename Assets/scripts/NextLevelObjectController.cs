﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NextLevelObjectController : MonoBehaviour {

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

	}

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (Input.GetKeyDown(KeyCode.F))
            GameManager.Instance.LoadNextLevel();
    }
}
