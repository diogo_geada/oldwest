﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Effects;

public interface IItem
{
    #region PROPERTIES

    GameItemType ItemType { get; }

    string VisibleName { get; }

    string SpriteName { get; }

    EffectType EffectType { get; }

    Effect Effect { get; }

    float Price { get; }

    StoreType StoreType { get; }

    /// <summary>
    /// This tells if the item goes to the bag (collectable) or consumed right away (non-collectable). If it's non-collectable there is no need to set the Stackable property.
    /// </summary>
    bool Collectible { get;  }

    bool Stackable { get; }

    bool OneTimeBuy { get; }

    #endregion
}
