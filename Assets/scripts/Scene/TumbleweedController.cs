﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent ( typeof ( Rigidbody2D))]
public class TumbleweedController : MonoBehaviour {

    private Rigidbody2D rb;
    Vector2 speed = new Vector2(-1, 0);

	// Use this for initialization
	void Awake () {
        rb = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
        rb.velocity = speed;
	}
}
