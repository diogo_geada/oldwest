﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tumbleweedSpawner : MonoBehaviour {

    public Vector2 generationInterval = new Vector2(20, 120);
    public Vector2 generationQuantity = new Vector2(1, 5);
    public Vector2 generationCoordinates = new Vector2(0, 0);
    public float generationHeight = 1f;

    public GameObject tumbleweedPrefab;

	// Use this for initialization
	void Start () {
        StartCoroutine(Generate());
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    //Creates tumbleweeds for ambient improvement
    private IEnumerator Generate()
    {

        int count = (int) Random.Range(generationQuantity.x, generationQuantity.y);

        for (int i = 0; i < count; i++)
        {
            GameObject temp = Instantiate(tumbleweedPrefab);
            temp.transform.position = new Vector2(Random.Range(generationCoordinates.x, generationCoordinates.y), generationHeight);
        }

        yield return new WaitForSeconds(Random.Range(generationInterval.x, generationInterval.y));

        StartCoroutine(Generate());
    }


}
