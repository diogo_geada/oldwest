﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(CombatDurabilityController))]
public class coverController : MonoBehaviour {

    private int defaultLayer;
    private int characterLayer;
    public int rendererHiddenLayer;
    public int rendererDefaultLayer;

    public List<GameObject> objectsInRange = new List<GameObject>();

    public GameObject canvas;
    public Text hintText;
    public KeyType interactionKey;
    private KeyCode key;

    private GameObject character;

    public GameObject Character
    {
        get { return character; }
    }

    private CombatDurabilityController healthController;

    private SpriteRenderer characerSprite;
    private SpriteRenderer sprite;
    public Color hiddenColor;
    private Color defaultColor;

    private int isShooting = 0; //controlls the amount of shots the player fired in a row while in cover

    void Awake()
    {
        healthController = GetComponent<CombatDurabilityController>();
    }

    // Use this for initialization
    void Start () {
        key = GameManager.keys[interactionKey];
        sprite = GetComponent<SpriteRenderer>();
        hintText.text = key.ToString();
        defaultLayer = gameObject.layer;

        if(healthController != null)
            healthController.onDeath.Add( () => { Show(); });
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D other)
    {
        objectsInRange.Add(other.gameObject);
        if (other.gameObject.tag == "Player" && !character)
        {
            canvas.SetActive(true);
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        objectsInRange.Remove(other.gameObject);
        if (other.gameObject.tag == "Player")
        {
            canvas.SetActive(false);
        }

        if(other.gameObject == character)
        {
            Show();
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player" && !character && Input.GetKeyDown(key))
        {
            Hide(collision.gameObject);
        }
    }

    //to avoid bugs on cover, the cactus goes to player layer, that way the cactus will stop the bullets if they hit the cactus
    //before they hit the player
    public bool Hide(GameObject toHide)
    {
        canvas.SetActive(false);
        //if there is someone already hidding here or the character toHide is null, return
        if (!toHide || character) return false;

        //if the character is in range to hide
        if (objectsInRange.IndexOf(toHide) == -1) return false;

        //prepare character
        character = toHide;
        characerSprite = character.GetComponent<SpriteRenderer>();
        character.GetComponent<ShootController>().onShootStart.Add(() => onShootStart());//subscribe onShootStart Event
        character.GetComponent<ShootController>().onShootEnd.Add(() => onShootEnd());//subscribe onShootEnd Event

        //hide character
        defaultColor = characerSprite.color;
        SetInvulnerable();
        return true;
    }

    public void Show()
    {
        //if there is someone already hidding here or the character toShow is null, return
        if (!character) return;
        SetVulnerable();

        character.GetComponent<ShootController>().onShootStart.Clear();//unsubscribe onShootStart Event
        character.GetComponent<ShootController>().onShootEnd.Clear();//unsubscribe onShootEnd Event

        EnemyController cont = character.GetComponent<EnemyController>();
        if (cont != null) cont.Cover = Enums.CoverState.exposed;

        characerSprite = null;
        character = null;

    }

    //While the Character is shooting, he gets vulnerable to bullets
    public void onShootStart()
    {
        isShooting++;
        SetVulnerable();
    }

    public void onShootEnd()
    {
        isShooting--;
        if (isShooting > 0) return;

        SetInvulnerable();
    }

    /// <summary>
    /// Shows player, ends cover
    /// </summary>
    public void SetVulnerable()
    {
        if (!characerSprite || !sprite) return;
        gameObject.layer = defaultLayer;
        characerSprite.color = defaultColor;
        sprite.sortingOrder = rendererDefaultLayer;//sets cactus behind the player
    }

    /// <summary>
    /// Hides player, starts cover
    /// </summary>
    public void SetInvulnerable()
    {
        if (!character) return;

        gameObject.layer = (character) ? character.layer : defaultLayer;
        characerSprite.color = hiddenColor;
        sprite.sortingOrder = rendererHiddenLayer;//sets cactus in front of player
    }
}
