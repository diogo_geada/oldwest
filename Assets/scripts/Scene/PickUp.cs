﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PickUp : HintTextController {

    public GameItem item;
    public Vector2 quantity = new Vector2(1, 2);
    public bool destroy = true;

    [SerializeField]
    private Text hintText;                              //text to hint player on which key to press
    public KeyType interactionKey;                          //set key on unity inspector
    private KeyCode key;

    private GameObject player;

    public enum PickUpType
    {
        health,
        maxHealth,
        ammo,
        item
    }

    // Use this for initialization
    private void Start () {

        //setup interaction key
        key = GameManager.keys[KeyType.interact];
        hintText.text = key.ToString();
        
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerStay2D(Collider2D col) {
        if (player && Input.GetKeyDown(key)) {
            Consume();
        }
    }

   protected override void OnTriggerEnter2D(Collider2D col)
    {
        base.OnTriggerEnter2D(col);
        if (col.transform.tag == "Player") {
            player = col.gameObject;
        }

    }

    protected override void OnTriggerExit2D(Collider2D col)
    {
        base.OnTriggerExit2D(col);
        if (col.transform.tag == "Player")
        {
            player = null;
        }

    }

    void Consume() {
        int itens = (int) Random.Range(quantity.x, quantity.y);

        for (int i = 0; i < itens; i++)
        {
            switch (item.itemType)
            {
                case GameItemType.Bullet:
                    PlayerManager.Instance.Controller.shootController.StoredAmmo += itens;
                    break;
                default:
                    InventorySystem.Instance.AddItem(item);
                    break;
            }
            
        }

        HideHintText();

        if (destroy) Destroy(gameObject);
        else Destroy(this);
    }

}
