﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Destroyes any item that collides with it.
/// Prevents object spam
/// </summary>
public class Destroyer : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Destroy(collision.gameObject);
    }
}
