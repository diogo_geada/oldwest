﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BgScrollController : MonoBehaviour {

    public Transform[] backgrounds;
    private float[] paralaxScales;
    private float smoothing = 1f;

    private Transform cam;
    private Vector3 lastCamPos;

	// Use this for initialization
	void Start () {
        cam = Camera.main.transform;
        lastCamPos = cam.transform.position;

        paralaxScales = new float[backgrounds.Length];
        for (int i = 0; i < backgrounds.Length; i++)
        {
            paralaxScales[i] = backgrounds[i].position.z * -1;
        }
    }

    private void Update()
    {
        if (!cam) return;

        for (int i = 0; i < backgrounds.Length; i++)
        {
            float paralax = (lastCamPos.x - cam.position.x) * paralaxScales[i];

            float BackgroundTargetPosX = backgrounds[i].position.x + paralax;

            Vector3 targetPos = new Vector3(BackgroundTargetPosX, backgrounds[i].transform.position.y, backgrounds[i].transform.position.z);

            backgrounds[i].position = Vector3.Lerp(backgrounds[i].transform.position, targetPos, smoothing*Time.deltaTime);
        }

        lastCamPos = cam.position;
    }
}