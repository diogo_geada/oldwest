﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombatDurabilityController : HealthController {

	// Use this for initialization
	protected override void Start () {

        base.Start();

        healthLimit = (int)transform.lossyScale.y * 10;
        MaxHealth = healthLimit;

        onDeath.Add( () => { Destroy(gameObject); });

	}

}
