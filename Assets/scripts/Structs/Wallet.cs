﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Wallet {

    [SerializeField]
    private float money;

    public delegate void NewEvent();
    public NewEvent onRemove;
    public NewEvent onAdd;

    public float Money { get { return money; } }

    public Wallet()
    {
        money = 0f;
    }

    public Wallet(float startMoney)
    {
        money = startMoney;
    }

    public void AddMoney(float val)
    {
        money += val;
        onAdd();
    }

    public void RemoveMoney(float val)
    {
        money -= val;
        onRemove();
    }

    public void EmptyWallet()
    {
        money = 0;
    }


}
