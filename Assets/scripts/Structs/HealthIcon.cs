﻿using UnityEngine;
using UnityEngine.UI;


[System.Serializable]
public class HealthIcon
{
    [SerializeField]
    public Image icon;
    public GameObject life;
    [SerializeField]
    private bool active = true;

    public bool IsActive
    {
        get
        {
            return active;
        }

        set
        {
            active = value;
            icon.gameObject.SetActive(value);
        }
    }

    public HealthIcon(Image newIcon)
    {
        icon = newIcon;
    }
}
