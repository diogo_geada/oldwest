﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Effects
{
    [Serializable]
    public class Effect
    {
        public EffectType type;
        public int duration;
        public int timer = 0;
        public bool applied = false;

        //to safe default values
        public float safeGuard = 0f;

        //amount to add per second
        public float intensity;
    }
}
